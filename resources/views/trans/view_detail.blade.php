@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">ขนส่ง {{ $tranm->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/trans/index') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        
                        
                        <br/>

                        <div class="row">
                            <div class="col-md-4"><b>วันที่ Order/วันที่ส่ง</b> : {{ $tranm->orderm->order_date }} / {{ $tranm->orderm->order_date }}</div>
                            <div class="col-md-4"><b>ลูกค้า/สาขา</b> : {{ $tranm->orderm->customer->name }} / {{ $tranm->orderm->customerloc->name }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><b>รถ</b> : {{ $tranm->car->license_no or "-" }}</div>
                            <div class="col-md-4"><b>คนขับ/คนสนับสนุน</b> : {{ $tranm->driver->nickname or "-" }} / {{ $tranm->support->nickname or "-" }}</div>
                            <div class="col-md-4"><b>Status</b> : {{ $tranm->status }}</div>
                        </div>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>สินค้า</th>
                                        <th>จำนวน</th>
                                        <th>ราคาต่อหน่วย</th>
                                        <th>ราคา</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $totalprice = 0;
                                    @endphp
                                    @foreach ($tranm->trand as $item)
                                       <tr>
                                        <td>{{ $item->orderd->product->productgroup->name }} / {{ $item->orderd->product->name }}</td>
                                        <td>{{ $item->orderd->value }}</td>
                                         
                                        @if(!empty($item->orderd->product->productpricebylocid($tranm->orderm->customerloc->id)))
                                            <td>
                                                {{ $item->orderd->product->productpricebylocid($tranm->orderm->customerloc->id)->base_price or 0 }}
                                            </td>
                                            <td>
                                                {{ number_format($item->orderd->product->productpricebylocid($tranm->orderm->customerloc->id)->base_price * $item->orderd->value,2) }}
                                            </td>
                                            @php
                                                $totalprice += $item->orderd->product->productpricebylocid($tranm->orderm->customerloc->id)->base_price * $item->orderd->value
                                            @endphp
                                        @else
                                            <td>0.00</td>
                                            <td>0.00</td>
                                        @endif    
                                        </td>
                                    </tr> 
                                    @endforeach
                                    <tr>
                                        <td colspan="3"><b>Total</b></td>
                                        <td><b>{{ number_format($totalprice,2) }}</b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
