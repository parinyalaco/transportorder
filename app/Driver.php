<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = [
        'fname', 'lname', 'nickname', 'lacoid', 'citizenid', 'licenseid', 'note'
    ];
}
