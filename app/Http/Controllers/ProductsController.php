<?php

namespace App\Http\Controllers;

use App\CustomerLoc;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\ProductGroup;
use App\Unit;
use App\Package;
use App\ProductPrice;
use App\StockM;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        $mydate = date('Y-m-d');

        $data = $this->showstock($mydate);

        if (!empty($keyword)) {
            $products = Product::latest()->paginate($perPage);
        } else {
            $products = Product::latest()->paginate($perPage);
        }

        return view('products.index', compact('products', 'data','mydate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $productgrouplist  = ProductGroup::pluck('name', 'id');
        $unitlist  = Unit::pluck('name', 'id');
        $packagelist  = Package::pluck('name', 'id');
        return view('products.create',compact('productgrouplist', 'unitlist', 'packagelist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        Product::create($requestData);

        return redirect('products')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $productgrouplist  = ProductGroup::pluck('name', 'id');
        $unitlist  = Unit::pluck('name', 'id');
        $packagelist  = Package::pluck('name', 'id');

        return view('products.show', compact('product', 'productgrouplist', 'unitlist', 'packagelist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $productgrouplist  = ProductGroup::pluck('name', 'id');
        $unitlist  = Unit::pluck('name', 'id');
        $packagelist  = Package::pluck('name', 'id');

        return view('products.edit', compact('product', 'productgrouplist', 'unitlist', 'packagelist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $product = Product::findOrFail($id);
        $product->update($requestData);

        return redirect('products')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return redirect('products')->with('flash_message', ' deleted!');
    }

    public function setprice($id){
        $product = Product::findOrFail($id);
        $customerloclist = CustomerLoc::orderBy('customer_id')->get();

        $customerlistar = array();
        foreach ($customerloclist as $customerlocObj) {
            $customerlistar[$customerlocObj->customer_id][] = $customerlocObj;
        }

        $productpriceList = array();
        foreach ($product->productprice as $productPriceObj) {
            $productpriceList[$productPriceObj->customer_loc_id] = $productPriceObj;
        }

        return view('products.setprice', compact('product', 'customerloclist', 'productpriceList', 'customerlistar', 'customerlist'));
    }

    public function setpriceAction(Request $request, $id){
        $requestData = $request->all();
        $product = Product::findOrFail($id);
        $customerloclist = CustomerLoc::pluck('name', 'id');

        foreach ($customerloclist as $key => $value) {
            //if(isset($requestData['price'. $key]) && $requestData['price' . $key] > 0){
                $checkAndSave = ProductPrice::where('product_id', $id)
                    ->where('customer_loc_id', $key)
                    ->first();
                if(empty($checkAndSave)){
                    $tmp = array();
                    $tmp['product_id'] = $id;
                    $tmp['customer_loc_id'] = $key;
                    $tmp['base_price'] = $requestData['price' . $key];

                    ProductPrice::create($tmp);
                }else{
                    $checkAndSave->base_price = $requestData['price' . $key];
                    $checkAndSave->update();
                }
            //}
        }

        return redirect('products')->with('flash_message', ' added!');
    }

    public function showstock($date)
    {
        $productList = Product::pluck('id');
        $products = Product::all();

        $stocklist = StockM::where('stock_datetime', '<=', $date . " 00:00:00.000")->pluck('id');


        //var_dump($stocklist);
        $dataRw = DB::table('stock_ds')
            ->whereIn('stock_ds.stock_m_id', $stocklist)
            ->whereIn('stock_ds.product_id', $productList)
            ->groupBy('stock_ds.product_id')
            ->select(DB::raw('stock_ds.product_id as product_id,sum(stock_ds.value) as stocksum'))
            ->get();

        $data = array();

        //var_dump($dataRw);

        foreach ($dataRw as $stockdateobj) {
            $data[$stockdateobj->product_id] = $stockdateobj->stocksum;
        }

        return $data;
    }

    public function getprice($customer_loc_id){
        $dataraw = ProductPrice::where('customer_loc_id', $customer_loc_id)->get();

        $data2 = array();
        foreach ($dataraw as $productpriceObj) {
            $data2[$productpriceObj->product_id] = number_format((float) $productpriceObj->base_price, 2, '.', '');
        }

        $dataraw2 = Product::all();
        $data = array();
        foreach ($dataraw2 as $productObj) {
            if(isset($data2[$productObj->id])){
                $data[$productObj->id] = $data2[$productObj->id];
            }else{
                $data[$productObj->id] = "0.00";
            }
        }

        return json_encode($data);
    }

    public function getstock($date)
    {
        $productList = Product::pluck('id');
        $products = Product::all();

        $stocklist = StockM::where('stock_datetime', '<=', $date . " 00:00:00.000")->pluck('id');


        //var_dump($stocklist);
        $dataRw = DB::table('stock_ds')
            ->whereIn('stock_ds.stock_m_id', $stocklist)
            ->whereIn('stock_ds.product_id', $productList)
            ->groupBy('stock_ds.product_id')
            ->select(DB::raw('stock_ds.product_id as product_id,sum(stock_ds.value) as stocksum'))
            ->get();

        $data = array();

        //var_dump($dataRw);

        foreach ($dataRw as $stockdateobj) {
            $data[$stockdateobj->product_id] = $stockdateobj->stocksum;
        }

        return json_encode($data);
    }
}
