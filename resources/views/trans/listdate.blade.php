@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">รายการส่งสินค้า</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-3 {{ $errors->has('process_datetime') ? 'has-error' : ''}}">
                                <label for="process_datetime" class="control-label">{{ 'วัน' }}</label>
                                <input class="form-control" name="process_datetime" type="date" id="process_datetime" value = "{{$date}}" >
                                {!! $errors->first('process_datetime', '<p class="help-block">:message</p>') !!}
                                <a href="{{ url('/reports/exporttransdate/' . $date) }}" title="Excel"><button class="btn btn-info btn-m"><i class="fa fa-print" aria-hidden="true"></i></button></a>
                                   
                            </div>
                        </div>
                        <div >
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>วันที่แผน/ส่ง</th>
                                        <th>ลูกค้า/สาขา</th>
                                        <th>รถ/คนขับ/ผู้ช่วย</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                        @foreach ($productlist as $pid => $pobj)
                                        <th></th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                         @php
                            $alltotal = array();
                        @endphp
                        @foreach ($displayList as $key=>$tranm)
                        @php
                            $subtotal = array();
                        @endphp
                        <tr> <td colspan="6">
                            @if (!empty($tranm[0]->car_id) && !empty($tranm[0]->driver_id))
                                {{ $tranm[0]->car->license_no or '-' }} / {{ $tranm[0]->driver->nickname or '-' }} / {{ $tranm[0]->support->nickname or '-' }} 
                                @php
                                    $mainstatus = array();
                                    foreach($tranm as $tranmObjcheck){
                                        $mainstatus[$tranmObjcheck->status][]= $tranmObjcheck->status;
                                    }
                                @endphp


                                @if (isset($mainstatus['Active']))
                                    <a href="{{ url('/trans/changeStatus/' . $date.'/'.$tranm[0]->car_id.'/Ready') }}" title="Active -> Ready"><button class="btn btn-info btn-sm"><i class="fa fa-circle-o" aria-hidden="true"></i></button></a> 
                                @else
                                    @if (isset($mainstatus['Ready']))
                                        <a href="{{ url('/trans/changeStatus/' . $date.'/'.$tranm[0]->car_id.'/Active') }}" title="Ready -> Active"><button class="btn btn-info btn-sm"><i class="fa fa-check-circle-o" aria-hidden="true"></i></button></a>
                                    @else
                                        @foreach ($mainstatus as $skey=>$svalue)
                                            , {{ $skey }} 
                                        @endforeach
                                    @endif
                                @endif
                            @else
                               ยังไม่จัดรถ
                            @endif
                            </td>
                            @foreach ($productlist as $pid => $pobj)
                                        <td>{{ $pobj }}</td>
                                        @endforeach
                        
                            </tr>
                            
                                
                                @php
                                $countItem = sizeof($tranm);
                                $looprun = 0;
                                $nextVal = 0;
                                $prevVal = 0;
                                @endphp
                                @foreach ($tranm as $item)
                                    <tr>
                                        <td>
                                            @if (!empty($item->car_id) && !empty($item->driver_id) )
                                                @php
                                                    if($countItem > 1){
                                                        if($looprun == 0){
                                                            $nextVal = $tranm[$looprun+1]->id;
                                                @endphp
                                                            <a href="{{ url('/trans/changeOrder/' . $item->id.'/'.$nextVal) }}" title="View CustomerLoc"><i class="fa fa-caret-square-o-down" aria-hidden="true"></i></a> 
                                                @php
                                                        }elseif($looprun == ($countItem-1)){
                                                            $prevVal = $tranm[$looprun-1]->id;
                                                @endphp
                                                            <a href="{{ url('/trans/changeOrder/' . $item->id.'/'.$prevVal) }}" title="View CustomerLoc"><i class="fa fa-caret-square-o-up" aria-hidden="true"></i></a> 
                                                @php
                                                        }else{
                                                            $nextVal = $tranm[$looprun+1]->id;
                                                            $prevVal = $tranm[$looprun-1]->id;
                                                @endphp
                                                            <a href="{{ url('/trans/changeOrder/' . $item->id.'/'.$nextVal) }}" title="View CustomerLoc"><i class="fa fa-caret-square-o-down" aria-hidden="true"></i></a> 
                                                            <a href="{{ url('/trans/changeOrder/' . $item->id.'/'.$prevVal) }}" title="View CustomerLoc"><i class="fa fa-caret-square-o-up" aria-hidden="true"></i></a> 
                                                @php
                                                        }
                                                    }
                                                @endphp
                                                
                                            @endif

                                            @php

                                    $tmpsubitem = array();
                                    foreach ($item->orderm->orderd as $orderdObj) {
                                        $tmpsubitem[$orderdObj->product_id] = $orderdObj;
                                    }
                                @endphp

                                            {{ $item->seq }}</td>
                                        <td>{{ $item->orderm->order_date }} / {{ $item->tran_date }}</td>
                                        <td>{{ $item->orderm->customer->name }} / {{ $item->orderm->customerloc->name }}</td>
                                        <td>{{ $item->car->license_no or '-' }} / {{ $item->driver->nickname or '-' }} / {{ $item->support->nickname or '-' }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>
                                            <a href="{{ url('/trans/viewDetail/' . $item->id) }}" title="View CustomerLoc"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/trans/edit/' . $item->id ) }}" title="Edit CustomerLoc"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/trans/delete/' . $item->id ) }}" title="Delete CustomerLoc" onclick="return confirm(&quot;Confirm delete?&quot;)"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>

                                           
                                        </td>
                                        @foreach ($productlist as $pid => $pobj)
                                        @if (isset($tmpsubitem[$pid]))
                                        @php
                                            if(isset($subtotal[$pid])){
                                                $subtotal[$pid] += $tmpsubitem[$pid]->value;
                                            }else{
                                                $subtotal[$pid] = $tmpsubitem[$pid]->value;
                                            } 
                                            if(isset($alltotal[$pid])){
                                                $alltotal[$pid] += $tmpsubitem[$pid]->value;
                                            }else{
                                                $alltotal[$pid] = $tmpsubitem[$pid]->value;
                                            }                                       
                                        @endphp

                                            <td><b>{{ $tmpsubitem[$pid]->value }}</b></td>
                                        @else
                                        @php
                                            if(!isset($subtotal[$pid])){
                                                $subtotal[$pid] = 0;
                                            }    
                                            if(!isset($alltotal[$pid])){
                                                $alltotal[$pid] = 0;
                                            }                                     
                                        @endphp
                                            <td>-</td>
                                        @endif
                                        
                                        @endforeach
                                    </tr>
                                    @php
                                        $looprun++;
                                    @endphp

                                @endforeach
                            <tr>
                                <td colspan="6">SubTotal (
                                    @if (!empty($tranm[0]->car_id) && !empty($tranm[0]->driver_id))
                                {{ $tranm[0]->car->license_no or '-' }} / {{ $tranm[0]->driver->nickname or '-' }}
                                @else
                                ยังไม่ได้จัดรถ 
                                    @endif
                                )</td>
                                @foreach ($productlist as $pid => $pobj)
                                        <td>
                                        @if ( $subtotal[$pid] != 0 )
                                            <b>{{ $subtotal[$pid] }}</b>
                                        @else
                                            -
                                        @endif
                                        </td>
                                        @endforeach
                            </tr>
                        @endforeach
                        <tr>
                                <td colspan="6">Total</td>
                                @foreach ($productlist as $pid => $pobj)
                                        <td>
                                        @if (isset($alltotal[$pid]) && $alltotal[$pid] != 0 )
                                            <b>{{ $alltotal[$pid] }}</b>
                                        @else
                                            -
                                        @endif
                                        </td>
                                        @endforeach
                            </tr>
                         </tbody>
                            </table>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
