<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'product_group_id', 'name', 'sname','sap_code', 'desc', 'unit_id', 'unit_weight', 'unitperpack', 'package_id', 'package_weight'
    ];

    public function productgroup()
    {
        return $this->belongsTo('App\ProductGroup', 'product_group_id', 'id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id', 'id');
    }

    public function package()
    {
        return $this->belongsTo('App\Package', 'package_id', 'id');
    }

    public function productprice()
    {
        return $this->hasMany('App\ProductPrice', 'product_id');
    }

    public function productpricebylocid($location_id){
        return $this->productprice()->where('customer_loc_id', $location_id)->first();
    }
}
