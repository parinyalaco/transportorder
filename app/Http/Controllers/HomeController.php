<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rolename = \Auth::user()->role->name;
        $todate = date('Y-m-d');
        if($rolename == 'sale'){
            return redirect('trans/listdate/'. $todate);
        }elseif ($rolename == 'driver') {
                return redirect('trans/listdelivery/' . $todate);
        } elseif ($rolename == 'report') {
            return redirect('dashboards');
        }else{
            return view('home');
        }
    }
}
