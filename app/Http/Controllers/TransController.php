<?php

namespace App\Http\Controllers;

use App\OrderM;
use App\OrderD;
use App\Customer;
use App\CustomerLoc;
use App\Car;
use App\Driver;
use App\Product;
use App\TranM;
use App\TranD;
use App\ActLog;
use App\StockM;
use App\StockD;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class TransController extends Controller
{
    public function moveOrderToTrans($order_m_id){
        $orderm = OrderM::findOrFail($order_m_id);

        if(!empty($orderm)){
            $tmpTranm = array();
            $tmpTranm['order_m_id'] = $order_m_id;
            $tmpTranm['tran_date'] = $orderm->order_date;
            $tmpTranm['car_id'] = 0;
            $tmpTranm['driver_id'] = 0;
            $tmpTranm['support_id'] = 0;
            $tmpTranm['seq'] = 0;
            $tmpTranm['note'] = '';
            $tmpTranm['start-time'] = \Carbon\Carbon::now();
            $tmpTranm['delivery-time'] = \Carbon\Carbon::now();
            $tmpTranm['status'] = 'Pending';

            $tranmid = TranM::create($tmpTranm)->id;

            foreach ($orderm->orderd as $orderdObj) {
                if($orderdObj->value > 0){
                    $tmpTrand = array();
                    $tmpTrand['tran_m_id'] = $tranmid;
                    $tmpTrand['order_d_id'] = $orderdObj->id;
                    $tmpTrand['accept_value'] = $orderdObj->value;
                    $tmpTrand['init_value'] = $orderdObj->value;
                    $tmpTrand['load_value'] = $orderdObj->value;
                    $tmpTrand['current_price'] = $orderdObj->curent_price;
                    $tmpTrand['status'] = 'Pending';

                    TranD::create($tmpTrand);
                }
            }
        }

        return redirect('orders/index')->with('flash_message', ' added!');
    }

    public function reMoveOrderToTrans($order_m_id)
    {
        $orderm = OrderM::findOrFail($order_m_id);
        $tranm = TranM::where('order_m_id',$order_m_id)->first();

        if (!empty($orderm) && !empty($tranm)) {
            //$tmpTranm = array();
            $tranm->order_m_id = $order_m_id;
            $tranm->tran_date = $orderm->order_date;
            //$tmpTranm['start-time'] = \Carbon\Carbon::now();
            //$tmpTranm['delivery-time'] = \Carbon\Carbon::now();
            //$tmpTranm['status'] = 'Pending';

            $tranm->update();

            //delete tranD not in OrderD

            foreach ($tranm->trand as $trandObj) {
                $chkOrderD = OrderD::where('id', $trandObj->order_d_id)->first();
                if(empty($chkOrderD)){
                    TranD::destroy($trandObj->id);
                }
            }


            foreach ($orderm->orderd as $orderdObj) {

                $chktrd = TranD::where('tran_m_id', $tranm->id)->where('order_d_id', $orderdObj->id)->first();

                if(empty($chktrd)){
                    $tmpTrand = array();
                    $tmpTrand['tran_m_id'] = $tranm->id;
                    $tmpTrand['order_d_id'] = $orderdObj->id;
                    $tmpTrand['accept_value'] = $orderdObj->value;
                    $tmpTrand['init_value'] = $orderdObj->value;
                    $tmpTrand['load_value'] = $orderdObj->value;
                    $tmpTrand['current_price'] = $orderdObj->current_price;
                    $tmpTrand['status'] =  $tranm->status;

                    TranD::create($tmpTrand);
                }else{
                    $chktrd->tran_m_id = $tranm->id;
                    $chktrd->order_d_id = $orderdObj->id;
                    $chktrd->accept_value = $orderdObj->value;
                    $chktrd->init_value = $orderdObj->value;
                    $chktrd->load_value = $orderdObj->value;
                    $chktrd->current_price = $orderdObj->current_price;
                    $chktrd->status = $tranm->status;

                    $chktrd->update();
                }

                
            }
        }

        return redirect('orders/index')->with('flash_message', ' added!');
    }


    public function index(Request $request)
    {
        $selectdate = $request->get('selectdate');
        $keyword = $request->get('search');
        $keyword2 = $request->get('search2');
        $status = $request->get('status');
        $perPage = 25;

        $tranmObj = new TranM();

        if (!empty($selectdate)) {
            $tranmObj = $tranmObj->where('tran_date',$selectdate);
        }

        if (!empty($status)) {
            $tranmObj = $tranmObj->where('status', $status);
        }

        if (!empty($keyword)) {

            $licenseidlist = Car::where('license_no','like','%'. $keyword.'%')->pluck('id');

            $tranmObj = $tranmObj->whereIn('car_id', $licenseidlist);
        }
        if (!empty($keyword2)) {

            $driverlist = Driver::where('nickname', 'like', '%' . $keyword2 . '%')->pluck('id');

            $tranmObj = $tranmObj->whereIn('driver_id', $driverlist);
        }

        $tranms = $tranmObj->orderBy('tran_date', 'desc')->paginate($perPage);

        $productlist  = Product::pluck('sname', 'id');
        return view('trans.index', compact('tranms', 'productlist'));
    }

    public function edit($tran_m_id){
        $tranm = TranM::findOrFail($tran_m_id);

        $carlist  = Car::pluck('license_no', 'id');
        $carlist->prepend('===== Select =====', '');
        $driverlist  = Driver::pluck('nickname', 'id');
        $driverlist->prepend('===== Select =====', '');

        $data = DB::table('tran_ms')
        ->select(DB::raw('tran_ms.car_id, tran_ms.driver_id , tran_ms.support_id'))
        ->groupBy(DB::raw('tran_ms.car_id, tran_ms.driver_id , tran_ms.support_id'))
        ->where('tran_ms.tran_date', $tranm->tran_date)
        ->get();

        $setlist = array();
         $setlist[''] = "===== Select =====";
        foreach ($data as $dataObj) {

            if(!empty($dataObj->car_id) || !empty($dataObj->driver_id) || !empty($dataObj->support_id)){
            $carstr = "";
            if($dataObj->car_id != 0 && isset($carlist[$dataObj->car_id])){
                $carstr = $carlist[$dataObj->car_id];
            }
            $driverstr = "";
            if ($dataObj->driver_id != 0 && isset($driverlist[$dataObj->driver_id])) {
                $driverstr = $driverlist[$dataObj->driver_id];
            }
            $supportstr = "";
            if (!empty($dataObj->support_id) && isset($driverlist[$dataObj->support_id])) {
                $supportstr = $driverlist[$dataObj->support_id];
            }

            $setlist[$dataObj->car_id."-". $dataObj->driver_id."-". $dataObj->support_id] = $carstr."/". $driverstr."/". $supportstr;
        
            }
        }
        //$setlist->prepend('===== Select =====', '');


        $tranms = TranM::where('tran_date', $tranm->tran_date)->get();

        return view('trans.edit', compact('tranm', 'carlist', 'driverlist', 'tranms', 'setlist'));
    }

    public function editAction(Request $request, $tran_m_id)
    {
        $requestData = $request->all();

        $requestData['status'] = 'Pending';
        if(($requestData['car_id'] != '' && $requestData['driver_id'] != '') || ($requestData['set_id'] != '')){
            $requestData['status'] = 'Active';

           if(!empty($requestData['set_id'])){
                $datset = explode("-", $requestData['set_id']);
                $requestData['car_id'] = $datset[0];
                $requestData['driver_id'] = $datset[1];
                $requestData['support_id'] = $datset[2];
           }
            
        

            $tranm = TranM::findOrFail($tran_m_id);
            
            if($tranm->seq == 0){
                    $lastTranm = TranM::where('tran_date', $requestData['tran_date'])
                        ->where('car_id', $requestData['car_id'])
                        ->where('driver_id', $requestData['driver_id'])
                        ->where('id', '<>', $tran_m_id)
                        ->orderBy('seq', 'desc')
                        ->first();

                    if(!empty($lastTranm)){
                        $requestData['seq'] = $lastTranm->seq + 1;
                    }else{
                        $requestData['seq'] = 1;
                    }
                   
            }
            var_dump($requestData);
            $tranm->update($requestData);

            TranD::where('tran_m_id', $tran_m_id)
                ->update(['status' => $requestData['status']]);
        }

       // return redirect('trans/index')->with('flash_message', ' Updated!');
    }

    public function listdate($date){
        $tranms = TranM::where('tran_date', $date)->orderby('car_id', 'desc')->orderby('seq', 'asc')->get();

        $displayList = array();
        foreach ($tranms as $tranm) {
            $displayList[$tranm->car_id][] = $tranm;
        }
        $productlist  = Product::pluck('sname', 'id');
        return view('trans.listdate', compact('displayList','date','productlist'));
    }

    public function changeOrder($tran_m_id,$swap_id){
        $tranm = TranM::findOrFail($tran_m_id);
        $stranm= TranM::findOrFail($swap_id);
        
        $tmpSeq = $tranm->seq;
        $tranm->seq = $stranm->seq;
        $stranm->seq = $tmpSeq;

        $tranm->update();
        $stranm->update();

        return redirect('trans/listdate/'. $tranm->tran_date)->with('flash_message', ' Change order!');
    }

    public function listdelivery($date)
    {
        $driver_id = \Auth::user()->driver_id;
        $tranms = TranM::where('tran_date', $date)
            ->where('driver_id', $driver_id)
            ->orderby('seq', 'asc')->get();

        $displayList = array();
        foreach ($tranms as $tranm) {
            $displayList[$tranm->car_id][] = $tranm;
        }
        $productlist  = Product::pluck('sname', 'id');
        return view('trans.listdelivery', compact('displayList', 'date', 'productlist'));
    }

    public function changeStatus($date,$car_id,$status){

        TranM::where('tran_date', $date)
            ->where('car_id', $car_id)
            ->update(['status' => $status]);

        $tranms = TranM::where('tran_date', $date)
            ->where('car_id', $car_id)
            ->orderby('seq', 'asc')->get();

        $displayList = array();
        foreach ($tranms as $tranm) {
            TranD::where('tran_m_id', $tranm->id)
                ->update(['status' => $status]);
        }
        return redirect('trans/listdate/' . $date)->with('flash_message', ' Change order!');
    }

    public function deliveryStatus($date, $car_id, $status)
    {
        $driver_id = \Auth::user()->driver_id; 

        TranM::where('tran_date', $date)
            ->where('car_id', $car_id)
            ->update(['status' => $status]);

        $tranms = TranM::where('tran_date', $date)
            ->where('car_id', $car_id)
            ->orderby('seq', 'asc')->get();

        $displayList = array();
        foreach ($tranms as $tranm) {
            TranD::where('tran_m_id', $tranm->id)
                ->update(['status' => $status]);
        }

        $tmpLog = array();
        $tmpLog['car_id'] = $car_id;
        $tmpLog['driver_id'] = $driver_id;
        $tmpLog['ref_id'] = 0;

        if($status == 'Delivery') {
            $tmpLog['task'] = 'Start Transport';
        }

        if ($status == 'Delivered') {
            $tmpLog['task'] = 'End Transport';
        }

        ActLog::create($tmpLog);

        return redirect('trans/listdelivery/' . $date)->with('flash_message', ' Change order!');
    }

    public function deliveryitem($tran_m_id){
        $tranm = TranM::findOrFail($tran_m_id);
        $statuslist = array(
            'Accept-ALL' => 'รับทั้งหมด',
            'Accept-Some' => 'รับบางส่วน',
            'Reject' => 'ไม่รับ' 
        );
        return view('trans.deliveryitem', compact('tranm', 'statuslist'));
    }

    public function deliveryitemAction(Request $request,$tran_m_id)
    {

        
        $requestData = $request->all();

        $stockDObjs = array();

        $tranm = TranM::findOrFail($tran_m_id);
        
        foreach ($tranm->trand as $trandObj) {
            $trandsave = TranD::findOrFail($trandObj->id);
            $trandsave->current_price = $trandsave->orderd->current_price;

            if($requestData['status'] == 'Reject'){
                $trandsave->accept_value = 0;
            }else{
                $trandsave->accept_value = $requestData['accept_value' . $trandObj->id];
            }
            $trandsave->grap = $trandsave->init_value - $trandsave->accept_value;
            $trandsave->status = $requestData['status'];
            $trandsave->update();

            if($trandsave->grap <> 0){
                $tmpStockD = array();
                $tmpStockD['product_id'] = $trandsave->orderd->product_id;
                $tmpStockD['value'] = $trandsave->grap;
                $stockDObjsp[] = $tmpStockD;
            }
        }

        $tranm->tran_date = $requestData['tran_date'];
        $tranm->status = $requestData['status'];
        $tranm->note = $requestData['note'];
        $tranm->update();

        if(!empty($stockDObjs)){

            $dataMDels = OrderM::where('ref_id', $tranm->id)->where('ref_from','GAP')->get();

            foreach ($dataMDels as $dataMDel) {
                foreach ($dataMDel->stockd() as $dataDDel) {
                    StockD::destroy($dataDDel->id);
                }
                StockM::destroy($dataMDel->id);
            }

            

            $tmp = array();
            $tmp['stock_datetime'] =  date('Y-m-d H:i:s.000');
            $tmp['log'] = "Order Date " . date('Y-m-d') . " Customer Loc id " . $tranm->order_m_id;
            $tmp['sap_link'] = "";
            $tmp['desc'] = "Order Date " . date('Y-m-d') . " Customer Loc id " . $tranm->order_m_id;
            $tmp['ref_id']  = $tranm->id;
            $tmp['ref_from'] = "GAP";

            $stockmid = StockM::create($tmp)->id;

            foreach ($stockDObjs as $stockDObj) {
                $stockDObj['stock_m_id'] = $stockmid;

                $stockdid = StockD::create($stockDObj)->id;
            }
        }

        $message = "ส่งของ ". $tranm->orderm->customer->name."/". $tranm->orderm->customer->name;
        if(isset($tranm->support_id)){
            $message .= " โดย " . $tranm->car->license_no . " / " . $tranm->driver->nickname . " / " . $tranm->support->nickname;
        }else{
            $message .= " โดย " . $tranm->car->license_no . " / " . $tranm->driver->nickname ;
        
        }
        $message .= " สถานะ ". $tranm->status;
        if(!empty($tranm->note)){
            $message .= " Note: " . $tranm->note;
        }
        
        \App\Helpers\AppHelper::instance()->sendtoline($message);

        return redirect('trans/listdelivery/' . $requestData['tran_date'])->with('flash_message', ' Change order!');
    }

    public function viewDetail($tran_m_id){
        $tranm = TranM::findOrFail($tran_m_id);
        $statuslist = array(
            'Accept-ALL' => 'รับทั้งหมด',
            'Accept-Some' => 'รับบางส่วน',
            'Reject' => 'ไม่รับ'
        );
        return view('trans.view_detail', compact('tranm', 'statuslist'));
    }

    public function delete($tran_m_id){
        TranD::where('tran_m_id', $tran_m_id)->delete();

        TranM::destroy($tran_m_id);

        return redirect('trans/index')->with('flash_message', ' deleted!');
    }

    public function loadItem($date,$car){
        $tranms = TranM::where('tran_date', $date)
            ->where('car_id', $car)
            ->get();

        return view('trans.load_item', compact('tranms'));

    }
}