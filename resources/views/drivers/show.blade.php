@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">คนขับรถ {{ $driver->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/drivers') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/drivers/' . $driver->id . '/edit') }}" title="Edit Driver"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('drivers' . '/' . $driver->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Driver" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $driver->id }}</td>
                                    </tr>
                                    
                                    <tr>
                                        <th>ชื่อ - สกุล</th><td>{{ $driver->fname }} {{ $driver->lname }}</td>
                                    </tr>
                                    <tr>
                                        <th>ชื่อเรียก - รหัสพนักงาน</th><td>{{ $driver->nickname }} - {{ $driver->lacoid }}</td>
                                    </tr>
                                    <tr>
                                        <th>เลขบัตรประชาชน - เลขใบขับขี่</th><td>{{ $driver->citizenid }} - {{ $driver->licenseid }}</td>
                                    </tr>
                                    <tr>
                                        <th>Note</th><td>{{ $driver->note }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
