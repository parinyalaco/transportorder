@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Stock {{ $stock->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/stocks') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/stocks/' . $stock->id . '/edit') }}" title="Edit Stock"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('stocks' . '/' . $stock->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Stock" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <div class="row">
                            <div class="col-md-3">
                                ID : {{ $stock->id }}
                            </div>
                            <div class="col-md-3">
                                Date Time : {{ $stock->stock_datetime }}
                            </div>
                            <div class="col-md-3">
                                log : {{ $stock->log }}
                            </div>
                            <div class="col-md-3">
                                SAP Link : {{ $stock->sap_link }}
                            </div>
                            <div class="col-md-12">
                                Desc: {{ $stock->desc }}
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>สินค้า</th>
                                        <th>จำนวน</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($stock->stockd as $item)
                                       <tr>
                                       <td>{{ $item->product->name }}</td>
                                        <td>{{ $item->value }}</td>
                                    </tr> 
                                    @endforeach
                                    
                                    
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
