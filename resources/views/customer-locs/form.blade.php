<div class="col-md-4  {{ $errors->has('customer_id') ? 'has-error' : ''}}">
        {!! Form::label('customer_id', 'ลูกค้า', ['class' => 'control-label']) !!}
        @if (isset($customerloc->customer_id))
            {!! Form::select('customer_id', $customerlist,$customerloc->customer_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('customer_id', $customerlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'สาขา' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $customerloc->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('sap_code') ? 'has-error' : ''}}">
    <label for="sap_code" class="control-label">{{ 'รหัสลูกค้า' }}</label>
    <input class="form-control" name="sap_code" type="text" id="sap_code" required value="{{ $customerloc->sap_code or ''}}" >
    {!! $errors->first('sap_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('code') ? 'has-error' : ''}}">
    <label for="code" class="control-label">{{ 'Code' }}</label>
    <input class="form-control" name="code" type="text" id="code" required value="{{ $customerloc->code or ''}}" >
    {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('contract_sap') ? 'has-error' : ''}}">
    <label for="contract_sap" class="control-label">{{ 'Contract SAP' }}</label>
    <input class="form-control" name="contract_sap" type="text" id="contract_sap" value="{{ $customerloc->contract_sap or ''}}" >
    {!! $errors->first('contract_sap', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'ผู้ติดต่อ และ หมายเหตุ' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $customerloc->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('cond') ? 'has-error' : ''}}">
    <label for="cond" class="control-label">{{ 'วันที่ส่ง Monday Tuesday Wednesday ... ' }}</label>
    <input class="form-control" name="cond" type="text" id="cond" required value="{{ $customerloc->cond or ''}}" >
    {!! $errors->first('cond', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('lat') ? 'has-error' : ''}}">
    <label for="lat" class="control-label">{{ 'Latitude' }}</label>
    <input class="form-control" name="lat" type="number" id="lat" value="{{ $customerloc->lat or ''}}" >
    {!! $errors->first('lat', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('long') ? 'has-error' : ''}}">
    <label for="long" class="control-label">{{ 'Longtitude' }}</label>
    <input class="form-control" name="long" type="number" id="long" value="{{ $customerloc->long or ''}}" >
    {!! $errors->first('long', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
