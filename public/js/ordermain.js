$(document).ready(function() {
    $('#process_datetime').change(function () {
        var url = "/transportorder/trans/listdate/" + $(this).val();
        $(location).attr('href', url);
    });

    $('#process_datetime_delivery').change(function () {
        var url = "/transportorder/trans/listdelivery/" + $(this).val();
        $(location).attr('href', url);
    });


    $('#customer_id').change(function () {
        if ($(this).val() != '') {
            var select = $(this).attr("id");
            var value = $(this).val();
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: '/transportorder/customers/getCustomerLocById',
                method: "POST",
                data: { select: select, value: value, _token: _token },
                success: function (result) {
                    $('#customer_loc_id').html(result);
                }
            })
        }
    });


    $('.caldate').change(function() {
        if ($("#customer_loc_id").val() != '') {
            var select = $("#customer_loc_id").attr("id");
            var value = $("#customer_loc_id").val();
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: '/transportorder/customer-locs/calnexttrans',
                method: "POST",
                data: { select: select, value: value, _token: _token },
                success: function (result) {
                    $('#order_date').val(result);
                    getorder();
                    getstock();
                }
            })
        }
    });

    $('.getorderlist').change(function(){
        getorder();
    });


    $('#stock_datetime').change(function () {
        window.location = "/transportorder/stocks/showstock/" + $(this).val();
    });

    $('.getprice').change(function(){
        if ($("#customer_loc_id").val() != '') {
            var select = $("#customer_loc_id").attr("id");
            var value = $("#customer_loc_id").val();
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: '/transportorder/products/getprice/'+value,
                method: "GET",
                //data: { select: select, value: value, _token: _token },
                success: function (result) {
                    //$('#order_date').val(result);
                    //getorder();
                    resultarray = $.parseJSON(result);
                    $.each(resultarray,function (key,value) {
                        $('#price_' + key).html(value);
                    });
                }
            })
        }
    });

    $('#order_date').change(function () {
        getstock();
    });

});

function getorder(){
    var dateval = $("#order_date").val();
    var carval = $("#car_id").val();
    var _token = $('input[name="_token"]').val();
    if ((dateval != '') && (carval != '')) {
        $.ajax({
            url: '/transportorder/orders/getOrderInDateInCar',
            method: "POST",
            data: { dateval: dateval, carval: carval, _token: _token },
            success: function (result) {
                $('#resultorder').html(result);
            }
        })
    }
}

function getstock(){
    if ($("#order_date").val() != '') {
        var value = $("#order_date").val();
        $.ajax({
            url: '/transportorder/products/getstock/' + value,
            method: "GET",
            //data: { select: select, value: value, _token: _token },
            success: function (result) {
                //$('#order_date').val(result);
                //getorder();
                resultarray = $.parseJSON(result);
                $.each(resultarray, function (key, value) {
                    //console.log(key + ' - ' + value);
                    $('#stock_' + key).html(value);
                });
            }
        })
    }
}
