<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActLog extends Model
{
    protected $fillable = [
        'car_id','driver_id','task','ref_id'
    ];

    public function car()
    {
        return $this->belongsTo('App\Car', 'car_id', 'id');
    }

    public function driver()
    {
        return $this->belongsTo('App\Driver', 'driver_id', 'id');
    }
}
