<?php

namespace App\Http\Controllers;

use App\StockM;
use App\StockD;
use App\Product;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class StocksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $stocks = StockM::latest()->paginate($perPage);
        } else {
            $stocks = StockM::latest()->paginate($perPage);
        }

        return view('stocks.index', compact('stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $productlist = Product::pluck('sname','id');
        return view('stocks.create',compact('productlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        $requestData['stock_datetime'] = \Carbon\Carbon::parse($requestData['stock_datetime'])->format('Y-m-d')." 00:00";

        $stockmid = StockM::create($requestData)->id;

        $productlist  = Product::all();
        foreach ($productlist as $productObj) {

            if (!empty($requestData['numpack_' . $productObj->id])) {
                $stockdObj = array();
                $stockdObj['stock_m_id'] = $stockmid;
                $stockdObj['product_id'] = $productObj->id;
                $stockdObj['value'] = $requestData['numpack_' . $productObj->id];

                StockD::create($stockdObj);
            }
        }

        return redirect('stocks')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $stock = StockM::findOrFail($id);

        return view('stocks.show', compact('stock'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $stock = StockM::findOrFail($id);
        $productlist = Product::pluck('sname', 'id');
        return view('stocks.edit', compact('stock', 'productlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $stock = StockM::findOrFail($id);

        $requestData['stock_datetime'] = \Carbon\Carbon::parse($requestData['stock_datetime'])->format('Y-m-d')." 00:00";

        $stock->update($requestData);

        $stockmid = $stock->id;

        StockD::where('stock_m_id', $stockmid)->delete();

        $productlist  = Product::all();
        foreach ($productlist as $productObj) {

            if (!empty($requestData['numpack_' . $productObj->id])) {
                $stockdObj = array();
                $stockdObj['stock_m_id'] = $stockmid;
                $stockdObj['product_id'] = $productObj->id;
                $stockdObj['value'] = $requestData['numpack_' . $productObj->id];

                StockD::create($stockdObj);
            }
        }

        

        return redirect('stocks')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        StockM::destroy($id);

        return redirect('stocks')->with('flash_message', ' deleted!');
    }

    public function showstock($date){
        $productList = Product::pluck('id');
        $products = Product::all();

        $stocklist = StockM::where('stock_datetime','<=',$date." 00:00:00.000")->pluck('id');


        //var_dump($stocklist);
        $dataRw = DB::table('stock_ds')
            ->whereIn('stock_ds.stock_m_id', $stocklist)
            ->whereIn('stock_ds.product_id', $productList)
            ->groupBy('stock_ds.product_id')
            ->select(DB::raw('stock_ds.product_id as product_id,sum(stock_ds.value) as stocksum'))
            ->get();

        $data = array();

        //var_dump($dataRw);

        foreach ($dataRw as $stockdateobj) {
            $data[$stockdateobj->product_id] = $stockdateobj->stocksum;
        }

        return view('stocks.showstock', compact('data', 'productlist', 'products','date'));

    }
}
