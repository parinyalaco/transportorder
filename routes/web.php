<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/dashboards', 'DashboardsController@index');

Route::resource('drivers', 'DriversController');
Route::resource('cars', 'CarsController');
Route::resource('customers', 'CustomersController');
Route::resource('customer-locs', 'CustomerLocsController');
Route::get('products/setprice/{id}', 'ProductsController@setprice');
Route::post('products/setpriceAction/{id}', 'ProductsController@setpriceAction');
Route::get('products/getprice/{customer_loc_id}', 'ProductsController@getprice');
Route::get('products/getstock/{date}', 'ProductsController@getstock');

Route::resource('products', 'ProductsController');
Route::resource('users', 'UsersController');
Route::resource('stocks', 'StocksController');

Route::get('stocks/showstock/{date}', 'StocksController@showstock');

Route::get('orders/index', 'OrdersController@index');
Route::get('orders/add', 'OrdersController@add');
Route::post('orders/addAction', 'OrdersController@addAction');
Route::get('orders/viewDetail/{order_m_id}', 'OrdersController@viewDetail');
Route::post('orders/getOrderInDateInCar', 'OrdersController@getOrderInDateInCar');
Route::get('orders/edit/{order_m_id}', 'OrdersController@edit');
Route::post('orders/editAction/{order_m_id}', 'OrdersController@editAction');
Route::get('orders/delete/{order_m_id}', 'OrdersController@delete');

Route::get('trans/index', 'TransController@index');
Route::get('trans/edit/{tran_m_id}', 'TransController@edit');
Route::post('trans/editAction/{tran_m_id}', 'TransController@editAction');
Route::get('trans/moveOrderToTrans/{order_m_id}', 'TransController@moveOrderToTrans');
Route::get('trans/reMoveOrderToTrans/{order_m_id}', 'TransController@reMoveOrderToTrans');
Route::get('trans/listdate/{date}', 'TransController@listdate');
Route::get('trans/listdelivery/{date}', 'TransController@listdelivery');
Route::get('trans/changeOrder/{tran_m_id}/{swap_id}', 'TransController@changeOrder');
Route::get('trans/changeStatus/{date}/{car_id}/{status}', 'TransController@changeStatus');
Route::get('trans/deliveryStatus/{date}/{car_id}/{status}', 'TransController@deliveryStatus');
Route::get('trans/deliveryitem/{tran_m_id}', 'TransController@deliveryitem');
Route::post('trans/deliveryitemAction/{tran_m_id}', 'TransController@deliveryitemAction');
Route::get('trans/viewDetail/{tran_m_id}', 'TransController@viewDetail');
Route::get('trans/delete/{tran_m_id}', 'TransController@delete');
Route::get('trans/loadItem/{date}/{car_id}', 'TransController@loadItem');


Route::post('customer-locs/calnexttrans', 'CustomerLocsController@calnexttrans');
Route::post('customers/getCustomerLocById', 'CustomersController@getCustomerLocById');

Route::get('reports/order_rpt', 'ReportsController@orderRpt');
Route::post('reports/order_rpt_action', 'ReportsController@orderRptAction');
Route::get('reports/tran_rpt', 'ReportsController@tranRpt');
Route::post('reports/tran_rpt_action', 'ReportsController@tranRptAction');
Route::get('reports/tran_price_rpt', 'ReportsController@tranPriceRpt');
Route::post('reports/tran_price_rpt_action', 'ReportsController@tranPriceRptAction');
Route::get('reports/exporttransdate/{date}', 'ReportsController@exporttransdate');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
