
<div class="col-md-4  {{ $errors->has('fname') ? 'has-error' : ''}}">
    <label for="fname" class="control-label">{{ 'ชื่อ' }}</label>
    <input class="form-control" name="fname" type="text" id="fname" required value="{{ $driver->fname or ''}}" >
    {!! $errors->first('fname', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('lname') ? 'has-error' : ''}}">
    <label for="lname" class="control-label">{{ 'สกุล' }}</label>
    <input class="form-control" name="lname" type="text" id="lname" required value="{{ $driver->lname or ''}}" >
    {!! $errors->first('lname', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('nickname') ? 'has-error' : ''}}">
    <label for="nickname" class="control-label">{{ 'ชื่อเรียก' }}</label>
    <input class="form-control" name="nickname" type="text" id="nickname" required value="{{ $driver->nickname or ''}}" >
    {!! $errors->first('nickname', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-4  {{ $errors->has('lacoid') ? 'has-error' : ''}}">
    <label for="lacoid" class="control-label">{{ 'รหัสพนักงาน 5 หลัก' }}</label>
    <input class="form-control" name="lacoid" type="text" id="lacoid" required value="{{ $driver->lacoid or ''}}" >
    {!! $errors->first('lacoid', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('citizenid') ? 'has-error' : ''}}">
    <label for="citizenid" class="control-label">{{ 'เลขบัตรประชาชน' }}</label>
    <input class="form-control" name="citizenid" type="text" id="citizenid" required value="{{ $driver->citizenid or ''}}" >
    {!! $errors->first('citizenid', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('licenseid') ? 'has-error' : ''}}">
    <label for="licenseid" class="control-label">{{ 'เลขใบขับขี่' }}</label>
    <input class="form-control" name="licenseid" type="text" id="licenseid" required value="{{ $driver->licenseid or ''}}" >
    {!! $errors->first('licenseid', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('note') ? 'has-error' : ''}}">
    <label for="note" class="control-label">{{ 'Note' }}</label>
    <textarea class="form-control" name="note" type="text" id="note">{{ $driver->note or ''}}</textarea>
    {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
</div>
<div class="  col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
