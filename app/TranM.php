<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TranM extends Model
{
    protected $fillable = [
        'order_m_id', 'tran_date','car_id','driver_id','support_id','seq','note','start-time','delivery-time','status'
    ];

    public function orderm()
    {
        return $this->belongsTo('App\OrderM', 'order_m_id', 'id');
    }

    public function car()
    {
        return $this->belongsTo('App\Car', 'car_id', 'id');
    }

    public function driver()
    {
        return $this->belongsTo('App\Driver', 'driver_id', 'id');
    }

    public function support()
    {
        return $this->belongsTo('App\Driver', 'support_id', 'id');
    }

    public function trand()
    {
        return $this->hasMany('App\TranD', 'tran_m_id');
    }
}
