<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

use App\OrderM;
use App\TranM;
use App\Product;

class ReportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function orderRpt(){
        
        return view('reports.order_rpt');

    }

    public function orderRptAction(Request $request){

        $requestData = $request->all();

        $productlist  = Product::orderBy('product_group_id')->pluck('sname', 'id');
        $data = OrderM::whereBetween('order_date', [$requestData['from_date'], $requestData['to_date']])->orderBy('order_date')->orderBy('customer_id')->orderBy('customer_loc_id')->get();
        
        $filename = "order_report_" . date('ymdHi');

        Excel::create($filename, function ($excel) use ($data, $productlist) {
            $excel->sheet('Orders', function ($sheet) use ($data, $productlist) {
                $sheet->loadView('exports.orderexcel')->with('data', $data)->with('productlist', $productlist);
            });
        })->export('xlsx');
    }

    public function tranRpt()
    {

        return view('reports.tran_rpt');
    }

    public function tranRptAction(Request $request)
    {

        $requestData = $request->all();

        $productlist  = Product::orderBy('product_group_id')->pluck('sname', 'id');
        $dataRaw = TranM::whereBetween('tran_date', [$requestData['from_date'], $requestData['to_date']])
            ->where('car_id','<>','')
            ->orderBy('tran_date')
            ->orderBy('car_id')
            ->orderBy('driver_id')
            ->orderBy('order_m_id')
            ->get();

        $data = array();
        foreach ($dataRaw as $dataObj) {
            $data[$dataObj->car_id][] = $dataObj;
        }

        $filename = "tran_report_" . date('ymdHi');

        Excel::create($filename, function ($excel) use ($data, $productlist) {
            foreach ($data as $key => $dataObj) {
                $excel->sheet('Sheet'.$key, function ($sheet) use ($dataObj, $productlist) {
                    $sheet->loadView('exports.tranexcel')->with('data', $dataObj)->with('productlist', $productlist);
                });
            }
        })->export('xlsx');
    }

    public function tranPriceRpt()
    {

        return view('reports.tran_price_rpt');
    }

    public function tranPriceRptAction(Request $request)
    {

        $requestData = $request->all();

        $productlist  = Product::orderBy('product_group_id')->pluck('sname', 'id');
        $productRaw  = Product::all();
        $productPrice = array();
        foreach ($productRaw as $productObj) {
            foreach ($productObj->productprice as $productpriceobj) {
                if(isset($productpriceobj->customer_loc_id)){
                    $productPrice[$productObj->id][$productpriceobj->customer_loc_id] = $productpriceobj->base_price;
                }
            }
        }

        $dataRaw = TranM::whereBetween('tran_date', [$requestData['from_date'], $requestData['to_date']])
            ->where('car_id', '<>', '')
            ->orderBy('tran_date')
            ->orderBy('car_id')
            ->orderBy('driver_id')
            ->orderBy('order_m_id')
            ->get();

        $data = array();
        foreach ($dataRaw as $dataObj) {
            //$data[$dataObj->car_id][] = $dataObj;
            $data[1][] = $dataObj;
        }

        $filename = "tran_report_" . date('ymdHi');

        Excel::create($filename, function ($excel) use ($dataRaw, $productlist,$productPrice) {
            //foreach ($data as $key => $dataObj) {
                $excel->sheet('Sheet', function ($sheet) use ($dataRaw, $productlist, $productPrice) {
                    $sheet->loadView('exports.tranpriceexcel')
                    ->with('data', $dataRaw)
                    ->with('productlist', $productlist)
                    ->with('productPrice', $productPrice);
                });
            //}
        })->export('xlsx');
    }




    public function exporttransdate($date)
    {
        $tranms = TranM::where('tran_date', $date)->orderby('car_id', 'desc')->orderby('seq', 'asc')->get();

        $displayList = array();
        foreach ($tranms as $tranm) {
            $displayList[$tranm->car_id][] = $tranm;
        }
        $productlist  = Product::pluck('sname', 'id');
       // return view('trans.listdate', compact('displayList', 'date', 'productlist'));

        $filename = "tran_report_". $date."-". date('ymdHi');

        Excel::create($filename, function ($excel) use ($displayList, $date, $productlist) {
            //foreach ($data as $key => $dataObj) {
            $excel->sheet('Sheet', function ($sheet) use ($displayList, $date, $productlist) {
                $sheet->loadView('exports.transdateexcel')
                ->with('displayList', $displayList)
                    ->with('date', $date)
                    ->with('productlist', $productlist);
            });
            //}
        })->export('xlsx');
    }
}
