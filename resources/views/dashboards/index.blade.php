@extends('layouts.dashboard')

@section('content')
<div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>Dashboard</h3></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-5">
<div id="piechart"></div>
                            </div>
                            <div class="col-md-7">
<div id="piechart2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart', 'bar']});
      google.charts.setOnLoadCallback(drawChart);
        google.charts.setOnLoadCallback(drawChart2);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Date', 'Number of Order'],
          @foreach ($orderData as $item)
        ['{{ $item->order_date }}',     {{ $item->countdata }}],
            @endforeach
        ]);

        var options = {
          title: 'จำนวน Order ทั้งหมด'
        };

        

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        function selectHandler() {
          var selectedItem = chart.getSelection()[0];
          if (selectedItem) {
            var topping = data.getValue(selectedItem.row, 0);
             location.href = '/transportorder/orders/index';
          }
        }

        google.visualization.events.addListener(chart, 'select', selectHandler);  

        chart.draw(data, options);
      }

      

      function drawChart2() {

        var data = google.visualization.arrayToDataTable([
        ['วันที่', 
        @foreach ($statusList as $listitem)
        '{{$listitem}}', 
        @endforeach
        { role: 'annotation' } ],
        @foreach ($tranData as $date=>$tranDataObj)
        ['{{ $date }}', 
        @foreach ($statusList as $listitem)
        @if (isset($tranDataObj[$listitem]))
        {{ $tranDataObj[$listitem]->countdata }},
        @else
        0,
        @endif
        @endforeach
        ''],
        @endforeach

      ]);

      var options = {
        title: 'จำนวน ขนส่ง ทั้งหมด',
        legend: {position: 'top', textStyle: {color: 'blue', fontSize: 12}},
        isStacked: true
      };
        var materialChart = new google.visualization.ColumnChart(document.getElementById('piechart2'));

        function selectHandler() {
          var selectedItem = materialChart.getSelection()[0];
          if (selectedItem) {
            var topping = data.getValue(selectedItem.row, 0);
              location.href = '/transportorder/trans/listdate/' + topping;
          }
        }

        google.visualization.events.addListener(materialChart, 'select', selectHandler);  


      materialChart.draw(data, options);

      }
    </script>
    </script>
@endsection