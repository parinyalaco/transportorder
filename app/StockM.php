<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockM extends Model
{
    protected $fillable = [
        'stock_datetime', 'log', 'sap_link', 'desc','ref_id','ref_from'
    ];

    public function stockd()
    {
        return $this->hasMany('App\StockD', 'stock_m_id');
    }

}
