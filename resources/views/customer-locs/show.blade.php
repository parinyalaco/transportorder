@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">สาขา {{ $customerloc->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/customer-locs') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/customer-locs/' . $customerloc->id . '/edit') }}" title="Edit CustomerLoc"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('customerlocs' . '/' . $customerloc->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete CustomerLoc" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $customerloc->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>Customer</th><td>{{ $customerloc->customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>สาขา</th><td>{{ $customerloc->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>รหัสลูกค้า</th><td>{{ $customerloc->sap_code }}</td>
                                    </tr>
                                    <tr>
                                        <th>Code</th><td>{{ $customerloc->code }}</td>
                                    </tr>
                                    <tr>
                                        <th>Contract SAP</th><td>{{ $customerloc->contract_sap }}</td>
                                    </tr>
                                    <tr>
                                        <th>รายละเอียด</th><td>{{ $customerloc->desc }}</td>
                                    </tr>
                                    <tr>
                                        <th>เงื่อนไข</th><td>{{ $customerloc->cond }}</td>
                                    </tr>
                                    <tr>
                                        <th>Location</th><td>{{ $customerloc->lat }},{{ $customerloc->long }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
