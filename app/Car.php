<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = [
        'license_no', 'driver_id', 'city', 'brand', 'type', 'desc', 'max_weight', 'cond', 'note'
    ];

    public function driver()
    {
        return $this->belongsTo('App\Driver', 'driver_id', 'id');
    }
}
