<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_group_id')->nullable();
            $table->string('name',200);
            $table->string('sap_code',100);
            $table->string('desc');
            $table->integer('unit_id')->nullable();
            $table->float('unit_weight')->nullable();
            $table->integer('unitperpack')->nullable();
            $table->integer('package_id')->nullable();
            $table->float('package_weight')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
