@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Order</div>
                    <div class="card-body">
                        <a href="{{ url('/orders/add') }}" class="btn btn-success btn-sm" title="Add New CustomerLoc">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <a href="{{ url('/reports/order_rpt') }}" class="btn btn-success btn-sm" title="Add New CustomerLoc">
                            <i class="fa fa-ticket" aria-hidden="true"></i> Export
                        </a>

                        <form method="GET" action="{{ url('/orders/index') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="row">
                                <input type="date" class="form-control col-md-3" name="selectdate" placeholder="Search..." value="{{ request('selectdate') }}">
                                
                                <input type="text" class="form-control col-md-3" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <select  class="form-control col-md-3" name="status" id="status">
                                    <option value="" 
                                    @if (empty(request('status')))
                                        selected
                                    @endif
                                    >All</option>
                                    <option value="Pending"
                                    @if (!empty(request('status')) && request('status') == "Pending")
                                        selected
                                    @endif
                                    >Pending</option>
                                    <option value="Active"
                                    @if (!empty(request('status')) && request('status') == "Active")
                                        selected
                                    @endif
                                    >Active</option>
                                </select>
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>วันที่</th>
                                        <th>ลูกค้า/สาขา</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                        @foreach ($productlist as $pid => $pobj)
                                        <th>{{ $pobj }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($orderms as $item)
                                @php

                                    $tmpsubitem = array();
                                    foreach ($item->orderd as $orderdObj) {
                                        $tmpsubitem[$orderdObj->product_id] = $orderdObj;
                                    }
                                @endphp
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->order_date }}</td>
                                        <td>{{ $item->customer->name }} / {{ $item->customerloc->name }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>
                                            @if ($item->tranm->count() > 0)
                                                <a href="{{ url('/trans/reMoveOrderToTrans/' . $item->id) }}" title="View CustomerLoc"><button class="btn btn-primary btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Move Again</button></a>
                                            @else
                                                <a href="{{ url('/trans/moveOrderToTrans/' . $item->id) }}" title="View CustomerLoc"><button class="btn btn-primary btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Move</button></a>
                                            @endif
                                            <a href="{{ url('/orders/viewDetail/' . $item->id) }}" title="View CustomerLoc"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/orders/edit/' . $item->id ) }}" title="Edit CustomerLoc"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            @if ($item->tranm->count() == 0)
                                            <a href="{{ url('/orders/delete/' . $item->id ) }}" title="Delete CustomerLoc" onclick="return confirm(&quot;Confirm delete?&quot;)"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>
                                            @endif
                                           
                                        </td>
                                        @foreach ($productlist as $pid => $pobj)
                                        @if (isset($tmpsubitem[$pid]))
                                            <td>{{ $pobj }}<br/>{{ $tmpsubitem[$pid]->value }}</td>
                                        @else
                                            <td>-</td>
                                        @endif
                                        
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $orderms->appends([
                                'search' => Request::get('search'),
                                'status' => Request::get('status'),
                                'selectdate' => Request::get('selectdate'),
                                ])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
