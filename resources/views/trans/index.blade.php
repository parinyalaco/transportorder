@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">รายการส่งสินค้า</div>
                    <div class="card-body">
                        <a href="{{ url('/reports/tran_rpt') }}" class="btn btn-success btn-sm" title="Add New CustomerLoc">
                            <i class="fa fa-ticket" aria-hidden="true"></i> Export
                        </a>
                        <a href="{{ url('/reports/tran_price_rpt') }}" class="btn btn-success btn-sm" title="Add New CustomerLoc">
                            <i class="fa fa-ticket" aria-hidden="true"></i> Export with Price
                        </a>
                        <form method="GET" action="{{ url('/trans/index') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="row">
                                <input type="date" class="form-control col-md-3" name="selectdate" placeholder="Search..." value="{{ request('selectdate') }}">
                                <input type="text" class="form-control col-md-3" name="search" placeholder="ทะเบียนรถ..." value="{{ request('search') }}">
                                <input type="text" class="form-control col-md-3" name="search2" placeholder="ชื่อคนขับ..." value="{{ request('search2') }}">
                                <select  class="form-control col-md-3" name="status" id="status">
                                    <option value="" 
                                    @if (empty(request('status')))
                                        selected
                                    @endif
                                    >All</option>
                                    <option value="Pending"
                                    @if (!empty(request('status')) && request('status') == "Pending")
                                        selected
                                    @endif
                                    >Pending</option>
                                    <option value="Active"
                                    @if (!empty(request('status')) && request('status') == "Active")
                                        selected
                                    @endif
                                    >Active</option>
                                    <option value="Accept-ALL"
                                    @if (!empty(request('status')) && request('status') == "Accept-ALL")
                                        selected
                                    @endif
                                    >Accept-ALL</option>
                                    <option value="Accept-Some"
                                    @if (!empty(request('status')) && request('status') == "Accept-Some")
                                        selected
                                    @endif
                                    >Accept-Some</option>
                                    <option value="Reject"
                                    @if (!empty(request('status')) && request('status') == "Reject")
                                        selected
                                    @endif
                                    >Reject</option>
                                    <option value="Delivery"
                                    @if (!empty(request('status')) && request('status') == "Delivery")
                                        selected
                                    @endif
                                    >Delivery</option>
                                </select>
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>วันที่แผน/ส่ง</th>
                                        <th>ลูกค้า/สาขา</th>
                                        <th>รถ/คนขับ/ผู้ช่วย</th>
                                        <th>จำนวน</th>
                                        <th>Status</th>

                                        <th>Actions</th>
                                        @foreach ($productlist as $pid => $pobj)
                                        <th>{{ $pobj }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($tranms as $item)
                                @php

                                    $tmpsubitem = array();
                                    foreach ($item->orderm->orderd as $orderdObj) {
                                        $tmpsubitem[$orderdObj->product_id] = $orderdObj;
                                    }
                                @endphp
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->orderm->order_date }} / 
                                        <a href="{{ url('/trans/listdate/'.$item->tran_date) }}"  title="จัดรถ {{$item->tran_date }}">{{ $item->tran_date }}</a>
                                        
                                        </td>
                                        <td>{{ $item->orderm->customer->name }} / {{ $item->orderm->customerloc->name }}</td>
                                        <td>{{ $item->car->license_no or '-' }} / {{ $item->driver->nickname or '-' }} / {{ $item->support->nickname or '-' }}</td>
                                        <td>{{ $item->orderm->orderd->count() }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>
                                            <a href="{{ url('/trans/viewDetail/' . $item->id) }}" title="View CustomerLoc"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/trans/edit/' . $item->id ) }}" title="Edit CustomerLoc"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/trans/delete/' . $item->id ) }}" title="Delete CustomerLoc" onclick="return confirm(&quot;Confirm delete?&quot;)"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>
                                        </td>
                                        @foreach ($productlist as $pid => $pobj)
                                        @if (isset($tmpsubitem[$pid]))
                                            <td>{{ $pobj }}<br/>{{ $tmpsubitem[$pid]->value }}</td>
                                        @else
                                            <td>-</td>
                                        @endif
                                        
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $tranms->appends([
                                'search' => Request::get('search'),
                                'search2' => Request::get('search2'),
                                'status' => Request::get('status'),
                                'selectdate' => Request::get('selectdate'),
                                ])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
