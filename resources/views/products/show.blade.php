@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">สินค้า {{ $product->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/products') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/products/' . $product->id . '/edit') }}" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('products' . '/' . $product->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Product" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $product->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>กลุ่มสินค้า</th><td>{{ $product->productgroup->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>สินค้า</th><td>{{ $product->name }} / {{ $product->sname }}</td>
                                    </tr>
                                    <tr>
                                        <th>SAP CODE</th><td>{{ $product->sap_code }}</td>
                                    </tr>
                                    <tr>
                                        <th>รายละเอียด</th><td>{{ $product->desc }}</td>
                                    </tr>
                                    <tr>
                                        <th>น้ำหนักต่อหน่วยย่อย</th><td>{{ $product->unit_weight }} {{ $product->unit->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>จำนวนหน่วยย่อยต่อPackage</th><td>{{ $product->unitperpack }} / {{ $product->package->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>น้ำหนักต่อPackage</th><td>{{ $product->package_weight }} kg</td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            
                        </div>
                        <div class="row">
                                @foreach ($product->productprice as $item)
                                    <div class="col-md-3">
                                        {{ $item->customerloc->customer->name }} / {{ $item->customerloc->name }} : {{ $item->base_price }}
                                    </div>    
                                @endforeach
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
