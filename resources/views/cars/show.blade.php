@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">ข้อมูลรถยนต์ {{ $car->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/cars') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/cars/' . $car->id . '/edit') }}" title="Edit Car"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('cars' . '/' . $car->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Car" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $car->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>ทะเบียนรถ</th><td>{{ $car->license_no }} {{ $car->city }}</td>
                                    </tr>
                                    <tr>
                                        <th>คนขับประจำ</th><td>{{ $car->driver->nickname }}</td>
                                    </tr>
                                    <tr>
                                        <th>ยี่ห้อ/ประเภท</th><td>{{ $car->brand }} {{ $car->type }}</td>
                                    </tr>
                                    <tr>
                                        <th>รายละเอียดรถ</th><td>{{ $car->desc }}</td>
                                    </tr>
                                    <tr>
                                        <th>Max load / จำนวนจุดที่ลงได้</th><td>{{ $car->max_weight }} / {{ $car->cond }}</td>
                                    </tr>
                                    <tr>
                                        <th>Note</th><td>{{ $car->note }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
