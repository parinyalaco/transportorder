<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    tr th {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    tr th.noborder {
        border: none;
        word-wrap: normal;
    }
     tr th.noborder-last {
        border: none;
        word-wrap: normal;
    }
    tr th.noborderr {
        border: none;
        text-align: right;
        word-wrap: break-word;
    }
    tr th.noborderc {
        border: none;
        text-align: center;
        word-wrap: break-word;
        font: bolder;
    }
    tr td {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    </style>
</head>
<body>
<table>
                                <thead>
                                    <tr>
                                    <th colspan="{{ 5+sizeof($productlist) }}">รายการส่งสินค้าประจำวันที่ {{ $date }}</th>
        </tr>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>วันที่แผน/ส่ง</th>
                                        <th>ลูกค้า/สาขา</th>
                                        <th>รถ/คนขับ/ผู้ช่วย</th>
                                        <th>Status</th>
                                        @foreach ($productlist as $pid => $pobj)
                                        <th></th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                         @php
                            $alltotal = array();
                        @endphp
                        @foreach ($displayList as $key=>$tranm)
                        @php
                            $subtotal = array();
                        @endphp
                        <tr> <td colspan="5">
                            @if (!empty($tranm[0]->car_id) && !empty($tranm[0]->driver_id))
                                {{ $tranm[0]->car->license_no or '-' }} / {{ $tranm[0]->driver->nickname or '-' }} / {{ $tranm[0]->support->nickname or '-' }} 
                                @php
                                    $mainstatus = array();
                                    foreach($tranm as $tranmObjcheck){
                                        $mainstatus[$tranmObjcheck->status][]= $tranmObjcheck->status;
                                    }
                                @endphp
                            @else
                               ยังไม่จัดรถ
                            @endif
                            </td>
                            @foreach ($productlist as $pid => $pobj)
                                        <td>{{ $pobj }}</td>
                                        @endforeach
                        
                            </tr>
                            
                                
                                @php
                                $countItem = sizeof($tranm);
                                $looprun = 0;
                                $nextVal = 0;
                                $prevVal = 0;
                                @endphp
                                @foreach ($tranm as $item)
                                    <tr>
                                        <td>

                                            @php

                                    $tmpsubitem = array();
                                    foreach ($item->orderm->orderd as $orderdObj) {
                                        $tmpsubitem[$orderdObj->product_id] = $orderdObj;
                                    }
                                @endphp

                                            {{ $item->seq }}</td>
                                        <td>{{ $item->orderm->order_date }} / {{ $item->tran_date }}</td>
                                        <td>{{ $item->orderm->customer->name }} / {{ $item->orderm->customerloc->name }}</td>
                                        <td>{{ $item->car->license_no or '-' }} / {{ $item->driver->nickname or '-' }} / {{ $item->support->nickname or '-' }}</td>
                                        <td>{{ $item->status }}</td>
                                        @foreach ($productlist as $pid => $pobj)
                                        @if (isset($tmpsubitem[$pid]))
                                        @php
                                            if(isset($subtotal[$pid])){
                                                $subtotal[$pid] += $tmpsubitem[$pid]->value;
                                            }else{
                                                $subtotal[$pid] = $tmpsubitem[$pid]->value;
                                            } 
                                            if(isset($alltotal[$pid])){
                                                $alltotal[$pid] += $tmpsubitem[$pid]->value;
                                            }else{
                                                $alltotal[$pid] = $tmpsubitem[$pid]->value;
                                            }                                       
                                        @endphp

                                            <td><b>{{ $tmpsubitem[$pid]->value }}</b></td>
                                        @else
                                        @php
                                            if(!isset($subtotal[$pid])){
                                                $subtotal[$pid] = 0;
                                            }    
                                            if(!isset($alltotal[$pid])){
                                                $alltotal[$pid] = 0;
                                            }                                     
                                        @endphp
                                            <td>-</td>
                                        @endif
                                        
                                        @endforeach
                                    </tr>
                                    @php
                                        $looprun++;
                                    @endphp

                                @endforeach
                            <tr>
                                <td colspan="5">SubTotal (
                                    @if (!empty($tranm[0]->car_id) && !empty($tranm[0]->driver_id))
                                {{ $tranm[0]->car->license_no or '-' }} / {{ $tranm[0]->driver->nickname or '-' }}
                                @else
                                ยังไม่ได้จัดรถ 
                                    @endif
                                )</td>
                                @foreach ($productlist as $pid => $pobj)
                                        <td>
                                        @if ( $subtotal[$pid] != 0 )
                                            <b>{{ $subtotal[$pid] }}</b>
                                        @else
                                            -
                                        @endif
                                        </td>
                                        @endforeach
                            </tr>
                        @endforeach
                        <tr>
                                <td colspan="5">Total</td>
                                @foreach ($productlist as $pid => $pobj)
                                        <td>
                                        @if (isset($alltotal[$pid]) && $alltotal[$pid] != 0 )
                                            <b>{{ $alltotal[$pid] }}</b>
                                        @else
                                            -
                                        @endif
                                        </td>
                                        @endforeach
                            </tr>
                         </tbody>
                            </table>
</body>
</html>
