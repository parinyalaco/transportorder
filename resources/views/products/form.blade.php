<div class="row">
<div class="col-md-3  {{ $errors->has('product_group_id') ? 'has-error' : ''}}">
        {!! Form::label('product_group_id', 'กลุ่มสินค้า', ['class' => 'control-label']) !!}
        @if (isset($product->product_group_id))
            {!! Form::select('product_group_id', $productgrouplist,$product->product_group_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('product_group_id', $productgrouplist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('product_group_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'ชื่อสินค้า' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $product->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('sname') ? 'has-error' : ''}}">
    <label for="sname" class="control-label">{{ 'ชื่อย่อ' }}</label>
    <input class="form-control" name="sname" type="text" id="sname" required value="{{ $product->sname or ''}}" >
    {!! $errors->first('sname', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('sap_code') ? 'has-error' : ''}}">
    <label for="sap_code" class="control-label">{{ 'SAP CODE' }}</label>
    <input class="form-control" name="sap_code" type="text" id="sap_code" required value="{{ $product->sap_code or ''}}" >
    {!! $errors->first('sap_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" id="desc">{{ $product->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('unit_id') ? 'has-error' : ''}}">
        {!! Form::label('unit_id', 'หน่วยย่อย', ['class' => 'control-label']) !!}
        @if (isset($product->unit_id))
            {!! Form::select('unit_id', $unitlist,$product->unit_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('unit_id', $unitlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('unit_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('unit_weight') ? 'has-error' : ''}}">
    <label for="unit_weight" class="control-label">{{ 'น้ำหนักต่อหน่วยย่อย' }}</label>
    <input class="form-control" name="unit_weight" type="text" id="unit_weight" value="{{ $product->unit_weight or ''}}" >
    {!! $errors->first('unit_weight', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('unitperpack') ? 'has-error' : ''}}">
    <label for="unitperpack" class="control-label">{{ 'จำนวนหน่วยย่อยต่อPackage' }}</label>
    <input class="form-control" name="unitperpack" type="text" id="unitperpack" value="{{ $product->unitperpack or ''}}" >
    {!! $errors->first('unitperpack', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('package_id') ? 'has-error' : ''}}">
        {!! Form::label('package_id', 'หน่วย Package', ['class' => 'control-label']) !!}
        @if (isset($product->package_id))
            {!! Form::select('package_id', $packagelist,$product->package_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('package_id', $packagelist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('package_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('package_weight') ? 'has-error' : ''}}">
    <label for="package_weight" class="control-label">{{ 'น้ำหนักต่อPackage (kg)' }}</label>
    <input class="form-control" name="package_weight" type="text" id="package_weight" value="{{ $product->package_weight or ''}}" >
    {!! $errors->first('package_weight', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>


</div>
