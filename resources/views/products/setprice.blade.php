@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">สินค้า {{ $product->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/products') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/products/' . $product->id . '/edit') }}" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('products' . '/' . $product->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Product" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/products/setpriceAction/' . $product->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3"><b>ID</b> : {{ $product->id }}</div>
                            <div class="col-md-3"><b>กลุ่มสินค้า</b> : {{ $product->productgroup->name }}</div>
                            <div class="col-md-3"><b>สินค้า</b> : {{ $product->name }} / {{ $product->sname }}</div>
                            <div class="col-md-3"><b>SAP CODE</b> : {{ $product->sap_code }}</div>
                            <div class="col-md-3"><b>รายละเอียด</b> : {{ $product->desc }}</div>
                            <div class="col-md-3"><b>น้ำหนักต่อหน่วยย่อย</b> : {{ $product->unit_weight }} {{ $product->unit->name }}</div>
                            <div class="col-md-3"><b>จำนวนหน่วยย่อยต่อPackage</b> : {{ $product->unitperpack }} / {{ $product->package->name }}</div>
                            <div class="col-md-3"><b>น้ำหนักต่อPackage</b> : {{ $product->package_weight }} kg</div>
                        </div>
                        <br/>
                        <div class="row">
                            <div id="accordion">
                            @foreach ($customerlistar as $key=>$jobarr)
                                <div class="card">
                                    <div class="card-header" id="heading{{$key}}">
                                    <h5 class="mb-{{$key}}">
                                        <button class="btn btn-warning" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}" onclick="return false;">
                                        {{ $jobarr[0]->customer->name }}
                                        </button>
                                    </h5>
                                    </div>
                                </div>

                                    <div id="collapse{{$key}}" class="collapse" aria-labelledby="heading{{$key}}" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            @foreach ($jobarr as $item)
                                            <div class="col-md-3">
                                                {{ $item->customer->name }} / 
                                                {{ $item->name }}<br/>
                                @php
                                    $currentvalue = 0;
                                    if(isset($productpriceList[$item->id])){
                                        $currentvalue = $productpriceList[$item->id]->base_price;
                                    }
                                @endphp
                                <input class="form-control" name="price{{$item->id}}" type="text" id="price{{$item->id}}" value="{{ number_format($currentvalue,2) }}" >
                            </div>
                                            @endforeach
                                                    </div>
                                    </div>

                                    </div>
                                    @endforeach

                            </div>
                           

                            
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input class="btn btn-primary" type="submit" value="Set Price">
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
