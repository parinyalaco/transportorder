<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderD extends Model
{
    protected $fillable = [
        'order_m_id','product_id','product_price_id','value','weight',
        'onhand_value', 'gap_value','current_price'
    ];

    public function orderm()
    {
        return $this->belongsTo('App\OrderM', 'order_m_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }

}
