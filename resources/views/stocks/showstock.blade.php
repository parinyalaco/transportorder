@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Show Stock วันที่ {{ $date }}</div>
                    <div class="card-body">
                        <a href="{{ url('/stocks') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <div class="row">
                            <div class="form-group col-md-3 {{ $errors->has('stock_datetime') ? 'has-error' : ''}}">
                                <label for="stock_datetime" class="control-label">{{ 'วัน' }}</label>
                                <input class="form-control" name="stock_datetime" type="date" id="stock_datetime" value = "{{$date}}" >
                                {!! $errors->first('stock_datetime', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            @php
                                $checkloop = intval(0);
                            @endphp
                            <div class="row padtopbuttom">
                            @foreach ($products as $item)
                                
                                <div class="col-md-3 "><b>{{ $item->name }}</b><br/>
                                @if (isset($data[$item->id]))
                                    {{ $data[$item->id] }} 
                                @else
                                    0
                                @endif
                                กล่อง
                                </div>

                                @php
                                    $checkloop++;
                                   // echo $checkloop . " " ;
                                @endphp
                                @if (($checkloop%4)==0)
                                    </div>
                                    <div class="row padtopbuttom">
                                @endif
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
