<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TranD extends Model
{
    protected $fillable = [
        'tran_m_id', 'order_d_id', 'accept_value', 'status',
        'load_value', 'init_value', 'gap_value', 'current_price'
    ];

    public function tranm()
    {
        return $this->belongsTo('App\TranM', 'tran_m_id', 'id');
    }

    public function orderd()
    {
        return $this->belongsTo('App\OrderD', 'order_d_id', 'id');
    }
}
