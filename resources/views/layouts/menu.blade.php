<div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                @php
                                    if(Auth::user()->role->name == 'admin'){
                                @endphp
                                    <li>
                                        <a href="{{route('users.index')}}">Users</a>
                                    </li>
                                    <li>
                                        <a href="{{route('cars.index')}}">รถยนต์</a>
                                    </li>
                                    <li>
                                        <a href="{{route('drivers.index')}}">คนขับรถ</a>
                                    </li>
                                    <li>
                                        <a href="{{route('customers.index')}}">ลูกค้า</a>
                                    </li>
                                    <li>
                                        <a href="{{route('customer-locs.index')}}">สาขา</a>
                                    </li>
                                    <li>
                                        <a href="{{route('products.index')}}">สินค้า</a>
                                    </li>
                                    <li><hr/></li>
                                    <li>
                                        <a href="{{route('stocks.index')}}">Stock</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('orders/index')}}">Order</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('trans/index')}}">จัดรถ</a>
                                    </li>
                                @php
                                    }elseif(Auth::user()->role->name == 'sale'){
                                @endphp
                                    <li>
                                        <a href="{{route('cars.index')}}">รถยนต์</a>
                                    </li>
                                    <li>
                                        <a href="{{route('drivers.index')}}">คนขับรถ</a>
                                    </li>
                                    <li>
                                        <a href="{{route('customers.index')}}">ลูกค้า</a>
                                    </li>
                                    <li>
                                        <a href="{{route('products.index')}}">สินค้า</a>
                                    </li>
                                    <li>
                                        <a href="{{route('stocks.index')}}">Stock</a>
                                    </li>
                                    <li>
                                        <a href="{{route('customer-locs.index')}}">สาขา</a>
                                    </li>
                                     <li><hr/></li>
                                    <li>
                                        <a href="{{ url('orders/index')}}">Order</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('trans/index')}}">จัดรถ</a>
                                    </li>
                                @php
                                    }elseif(Auth::user()->role->name == 'driver'){
                                @endphp
                                    <li>
                                        <a href="{{ url('trans/listdelivery/'.date('Y-m-d'))}}">รายการส่งวันนี้</a>
                                    </li>
                                @php
                                    }elseif(Auth::user()->role->name == 'report'){
                                @endphp
                                    <li>
                                        <a href="{{ url('orders/index')}}">Order</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('trans/index')}}">จัดรถ</a>
                                    </li>
                                @php
                                    }elseif(Auth::user()->role->name == 'planner'){
                                @endphp
                                      <li>
                                        <a href="{{route('cars.index')}}">รถยนต์</a>
                                    </li>
                                    <li>
                                        <a href="{{route('drivers.index')}}">คนขับรถ</a>
                                    </li>
                                    <li>
                                        <a href="{{route('customers.index')}}">ลูกค้า</a>
                                    </li>
                                    <li>
                                        <a href="{{route('customer-locs.index')}}">สาขา</a>
                                    </li>
                                     <li><hr/></li>
                                    <li>
                                        <a href="{{ url('orders/index')}}">Order</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('trans/index')}}">จัดรถ</a>
                                    </li>  
                                @php
                                    }
                                @endphp
                                     <li><hr/></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>