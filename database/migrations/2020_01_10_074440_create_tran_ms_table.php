<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_ms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_m_id');
            $table->date('tran_date');
            $table->integer('car_id')->nullable();
            $table->integer('driver_id')->nullable();
            $table->integer('seq');
            $table->string('note')->nullable();
            $table->dateTime('start-time')->nullable();
            $table->dateTime('delivery-time')->nullable();
            $table->string('status','20');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tran_ms');
    }
}
