<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'name', 'desc', 'creditterm'
    ];

    public function customerloc()
    {
        return $this->hasMany('App\CustomerLoc', 'customer_id');
    }
}
