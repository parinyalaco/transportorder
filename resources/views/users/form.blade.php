
<div class="col-md-4  {{ $errors->has('username') ? 'has-error' : ''}}">
    <label for="username" class="control-label">{{ 'Username' }}</label>
    <input class="form-control" name="username" type="text" id="username" required value="{{ $user->username or ''}}" >
    {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="email" id="email" required value="{{ $user->email or ''}}" >
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="control-label">{{ 'password' }}</label>
    <input class="form-control" name="password" type="password" id="password" required value="" >
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('role_id') ? 'has-error' : ''}}">
        {!! Form::label('role_id', 'คนขับ', ['class' => 'control-label']) !!}
        @if (isset($user->role_id))
            {!! Form::select('role_id', $rolelist,$user->role_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('role_id', $rolelist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('role_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('driver_id') ? 'has-error' : ''}}">
        {!! Form::label('driver_id', 'คนขับ', ['class' => 'control-label']) !!}
        @if (isset($user->driver_id))
            {!! Form::select('driver_id', $driverlist,$user->driver_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('driver_id', $driverlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('driver_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
