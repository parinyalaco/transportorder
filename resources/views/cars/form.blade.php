<div class="col-md-4  {{ $errors->has('license_no') ? 'has-error' : ''}}">
    <label for="license_no" class="control-label">{{ 'เลขทะเบียนรถ' }}</label>
    <input class="form-control" name="license_no" type="text" id="license_no" required value="{{ $car->license_no or ''}}" >
    {!! $errors->first('license_no', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('driver_id') ? 'has-error' : ''}}">
        {!! Form::label('driver_id', 'คนขับประจำ', ['class' => 'control-label']) !!}
        @if (isset($car->driver_id))
            {!! Form::select('driver_id', $driverlist,$car->driver_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('driver_id', $driverlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('driver_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('city') ? 'has-error' : ''}}">
    <label for="city" class="control-label">{{ 'จังหวัด' }}</label>
    <input class="form-control" name="city" type="text" id="city" required value="{{ $car->city or ''}}" >
    {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-4  {{ $errors->has('brand') ? 'has-error' : ''}}">
    <label for="brand" class="control-label">{{ 'ยี่ห้อ' }}</label>
    <input class="form-control" name="brand" type="text" id="brand" required value="{{ $car->brand or ''}}" >
    {!! $errors->first('brand', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-4  {{ $errors->has('type') ? 'has-error' : ''}}">
    <label for="type" class="control-label">{{ 'ประเภทรถ' }}</label>
    <input class="form-control" name="type" type="text" id="type" required value="{{ $car->type or ''}}" >
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียดรถ' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $car->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('max_weight') ? 'has-error' : ''}}">
    <label for="max_weight" class="control-label">{{ 'น้ำหนักบรทุก' }}</label>
    <input class="form-control" name="max_weight" type="number" id="max_weight" required value="{{ $car->max_weight or ''}}" >
    {!! $errors->first('max_weight', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('cond') ? 'has-error' : ''}}">
    <label for="cond" class="control-label">{{ 'จำนวนจุดที่ลงได้ต่อวัน' }}</label>
    <input class="form-control" name="cond" type="number" id="cond" required value="{{ $car->cond or ''}}" >
    {!! $errors->first('cond', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('note') ? 'has-error' : ''}}">
    <label for="note" class="control-label">{{ 'Note' }}</label>
    <textarea class="form-control" name="note" type="text" id="note">{{ $car->note or ''}}</textarea>
    {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
