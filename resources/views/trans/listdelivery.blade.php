@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">รายการส่งสินค้า</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-3 {{ $errors->has('process_datetime') ? 'has-error' : ''}}">
                                <label for="process_datetime_delivery" class="control-label">{{ 'วัน' }}</label>
                                <input class="form-control" name="process_datetime_delivery" type="date" id="process_datetime_delivery" value = "{{$date}}" >
                                {!! $errors->first('process_datetime_delivery', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div >
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>วันที่แผน/ส่ง</th>
                                        <th>ลูกค้า/สาขา</th>
                                        <th>รถ/คนขับ/ผู้ช่วย</th>
                                        <th>Status</th>
                                        @foreach ($productlist as $pid => $pobj)
                                        <th>{{ $pobj }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                        @foreach ($displayList as $key=>$tranm)
                        
                        
                            
                        
                        

                        <tr> <td colspan="{{ (6 + sizeof($productlist)) }}">
                            @if (!empty($tranm[0]->car_id) && !empty($tranm[0]->driver_id))
                                {{ $tranm[0]->car->license_no or '-' }} / {{ $tranm[0]->driver->nickname or '-' }}
                                @php
                                    $mainstatus = array();
                                    foreach($tranm as $tranmObjcheck){
                                        $mainstatus[$tranmObjcheck->status][]= $tranmObjcheck->status;
                                    }
                                @endphp
                                @if (isset($mainstatus['Ready'])) 
                                    <a href="{{ url('/trans/deliveryStatus/' . $date.'/'.$tranm[0]->car_id.'/Delivery') }}" title="Active -> Ready"><button class="btn btn-info btn-sm"><i class="fa fa-circle-o" aria-hidden="true"></i></button></a>
                                @else
                                    @foreach ($mainstatus as $skey=>$svalue)
                                            , {{ $skey }} 
                                    @endforeach
                                @endif
                            @else
                               ยังไม่จัดรถ
                            @endif
                            </td></tr>
                            
                                
                                @php
                                $countItem = sizeof($tranm);
                                $looprun = 0;
                                $nextVal = 0;
                                $prevVal = 0;
                                @endphp
                                @foreach ($tranm as $item)
                                    <tr>
                                        <td>
                                        @php

                                            $tmpsubitem = array();
                                            foreach ($item->orderm->orderd as $orderdObj) {
                                                $tmpsubitem[$orderdObj->product_id] = $orderdObj;
                                            }
                                        @endphp
                                            {{ $item->seq }}</td>
                                        <td>{{ $item->orderm->order_date }} / {{ $item->tran_date }}</td>
                                        <td>{{ $item->orderm->customer->name }} / {{ $item->orderm->customerloc->name }}</td>
                                        <td>{{ $item->car->license_no or '-' }} / {{ $item->driver->nickname or '-' }} / {{ $item->support->nickname or '-' }}</td>
                                        <td>
                                            @if ($item->status == 'Delivery')
                                                <a href="{{ url('/trans/deliveryitem/' . $item->id) }}" title="Accept or Reject Delivery"><button class="btn btn-info btn-sm">Delivery</button></a>
                                            @else
                                                <a href="{{ url('/trans/deliveryitem/' . $item->id) }}" title="Accept or Reject Delivery"><button class="btn btn-info btn-sm">{{$item->status}}</button></a>
                                            @endif
                                            
                                        </td>
                                        @foreach ($productlist as $pid => $pobj)
                                            @if (isset($tmpsubitem[$pid]))
                                                <td>{{ $pobj }}<br/>{{ $tmpsubitem[$pid]->value }}</td>
                                            @else
                                                <td>-</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                    @php
                                        $looprun++;
                                    @endphp
                                @endforeach
                        @endforeach
                         </tbody>
                            </table>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
