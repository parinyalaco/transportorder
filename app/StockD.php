<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockD extends Model
{
    protected $fillable = [
        'stock_m_id', 'product_id', 'value'
    ];

    public function stockm()
    {
        return $this->belongsTo('App\StockM', 'stock_m_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }

}
