@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Load สินค้าขึ้นรถ </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-3 {{ $errors->has('process_datetime') ? 'has-error' : ''}}">
                                <label for="process_datetime_delivery" class="control-label">{{ 'วัน' }}</label>
                                <input class="form-control" name="process_datetime_delivery" type="date" id="process_datetime_delivery" value = "{{$date}}" >
                                {!! $errors->first('process_datetime_delivery', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div >
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>วันที่แผน/ส่ง</th>
                                        <th>ลูกค้า/สาขา</th>
                                        <th>รถ/คนขับ/ผู้ช่วย</th>
                                        <th>Status</th>
                                        @foreach ($productlist as $pid => $pobj)
                                        <th>{{ $pobj }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                      
                         </tbody>
                            </table>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
