<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    tr th {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    tr th.noborder {
        border: none;
        word-wrap: normal;
    }
     tr th.noborder-last {
        border: none;
        word-wrap: normal;
    }
    tr th.noborderr {
        border: none;
        text-align: right;
        word-wrap: break-word;
    }
    tr th.noborderc {
        border: none;
        text-align: center;
        word-wrap: break-word;
        font: bolder;
    }
    tr td {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>ลำดับ</th>
                <th>รอบส่งมอบ</th>
                <th>รถ</th>
                <th>คนขับ</th>
                <th>รายชื่อลูกค้า</th>
                <th>PO</th>
                <th>เลขที่SO</th>
                <th>Billing</th>
                @foreach ($productlist as $pid => $pobj)
                <th>{{ $pobj }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @php
                $maxloop = 0;
            @endphp
            @foreach($data as $item)
            @php
                $maxloop = $loop->iteration;
                $tmpsubitem = array();
                foreach ($item->trand as $trandObj) {
                    $tmpsubitem[$trandObj->orderd->product_id] = $trandObj;
                }
            @endphp
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->tran_date }}</td>
                    <td>{{ $item->car->license_no }}</td>
                    <td>{{ $item->driver->nickname or '' }} / {{ $item->support->nickname or '' }} </td>
                    <td>{{ $item->orderm->customer->name }} / {{ $item->orderm->customerloc->name }}</td>
                    <td>{{ $item->orderm->po_code }}</td>
                    <td>{{ $item->orderm->so_code }}</td> 
                    <td>{{ $item->orderm->billing_code }}</td>
                    @foreach ($productlist as $pid => $pobj)
                    @if (isset($tmpsubitem[$pid]))
                        <td>{{ $tmpsubitem[$pid]->orderd->value }}</td>
                    @else
                        <td></td>
                    @endif
                    
                    @endforeach
                    
                </tr>
            @endforeach
            <tr>
                        <td colspan="8">Total</td>    
                        @php
                            $colloop = 9;
                        @endphp
                        @foreach ($productlist as $pid => $pobj)
                            <td>=subtotal(9, {{ \App\Helpers\AppHelper::instance()->getNameFromNumber($colloop) }}2:{{ \App\Helpers\AppHelper::instance()->getNameFromNumber($colloop) }}{{ $maxloop+1 }})</td>
                            @php
                                $colloop++;
                            @endphp
                        @endforeach
                    </tr>
        </tbody>
    </table>
</body>
</html>