<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_ms', function (Blueprint $table) {
            $table->increments('id');
            $table->date('order_date');
            $table->integer('customer_id');
            $table->integer('customer_loc_id');
            $table->integer('car_id');
            $table->integer('driver_id');
            $table->string('po_code', 50)->nullable();
            $table->string('so_code', 50)->nullable();
            $table->string('billing_code', 50)->nullable();
            $table->string('inv_code', 50)->nullable();
            $table->string('status', 50);
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_ms');
    }
}
