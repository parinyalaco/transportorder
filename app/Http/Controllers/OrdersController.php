<?php

namespace App\Http\Controllers;

use App\OrderM;
use App\OrderD;
use App\Customer;
use App\CustomerLoc;
use App\Car;
use App\Driver;
use App\Product;
use App\ProductPrice;
use App\StockM;
use App\StockD;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class OrdersController extends Controller
{

    public function index(Request $request)
    {
        $selectdate = $request->get('selectdate');
        $keyword = $request->get('search');
        $status = $request->get('status');
        $perPage = 25;


        $orderObj = new OrderM();

        if (!empty($selectdate)) {
            $orderObj = $orderObj->where('order_date', $selectdate);
        }

        if (!empty($status)) {
            $orderObj = $orderObj->where('status', $status);
        }

        if (!empty($keyword)) {

            $customerloclist = CustomerLoc::where('name','like','%'.$keyword.'%')->pluck('id');

            $orderObj = $orderObj->whereIn('customer_loc_id', $customerloclist);
        }

        $orderms = $orderObj->orderBy('order_date', 'desc')->paginate($perPage);

        $productlist  = Product::orderBy('product_group_id')->pluck('sname', 'id');

        return view('orders.index', compact('orderms', 'productlist'));
    }

    public function add(){
        $customerlist  = Customer::pluck('name', 'id');
        $customerlist->prepend('===== Select =====', '');
        $customerloclist  = CustomerLoc::pluck('name', 'id');
        $customerloclist->prepend('===== Select =====', '');
        $productlist  = Product::pluck('sname', 'id');
        return view('orders.add', compact('customerlist', 'customerloclist', 'productlist'));
    }

    public function addAction(Request $request){
        $requestData = $request->all();

        //Create main data 
        $orderMObj = array();

        $orderMObj['order_date'] = $requestData['order_date'];
        $orderMObj['customer_id'] = $requestData['customer_id'];
        $orderMObj['customer_loc_id'] = $requestData['customer_loc_id'];
        $orderMObj['po_code'] = $requestData['po_code'];
        $orderMObj['so_code'] = $requestData['so_code'];
        $orderMObj['billing_code'] = $requestData['billing_code'];
        $orderMObj['inv_code'] = $requestData['inv_code'];
        $orderMObj['status'] = 'Active';
        $orderMObj['note'] = $requestData['note'];
        
        $ordermid = OrderM::create($orderMObj)->id;

        $tmp = array();
        $tmp['stock_datetime'] =  $requestData['order_date']." 00:00:00.000";
        $tmp['log'] = "Order Date ". $requestData['order_date']." Customer Loc id ". $requestData['customer_loc_id'];
        $tmp['sap_link'] = ""; 
        $tmp['desc']= "Order Date " . $requestData['order_date'] . " Customer Loc id " . $requestData['customer_loc_id']; 
        $tmp['ref_id']  = $ordermid;
        $tmp['ref_from'] = "ORDER";

        $stockmid = StockM::create($tmp)->id;

       // $productpricelist = ProductPrice::where('customer_loc_id', $requestData['customer_loc_id'])->pluck('base_price','id');
        $productpricelist = ProductPrice::where('customer_loc_id', $requestData['customer_loc_id'])->pluck('base_price', 'product_id');


        $productlist  = Product::all();
        foreach ($productlist as $productObj) {

            if(!empty($requestData['numpack_'. $productObj->id])){
                $orderDObj = array();
                $orderDObj['order_m_id'] = $ordermid;
                $orderDObj['product_id'] = $productObj->id;
                $orderDObj['customer_loc_id'] = 0;
                $orderDObj['value'] = $requestData['numpack_'. $productObj->id];
                $orderDObj['weight'] = $productObj->package_weight * $requestData['numpack_'. $productObj->id];
                

                $orderDObj['current_price'] = 0;
                if(isset($productpricelist[$productObj->id])){
                    $orderDObj['current_price'] = $productpricelist[$productObj->id];

                }

                $stockDObj = array();

                $stockDObj['stock_m_id'] = $stockmid; 
                $stockDObj['product_id'] = $productObj->id;
                $stockDObj['value'] = -1*$requestData['numpack_' . $productObj->id];

                $orderdid = OrderD::create($orderDObj)->id;

                $stockdid = StockD::create($stockDObj)->id;

            }

            
        }

        return redirect('orders/index')->with('flash_message', ' added!');
    }

    public function viewDetail($order_m_id){
        
        $orderm = OrderM::findOrFail($order_m_id);

        return view('orders.view_detail',compact('orderm')); 
    }

    public function edit($order_m_id)
    {

        $order_m = OrderM::findOrFail($order_m_id);

        $customerlist  = Customer::pluck('name', 'id');
        $customerlist->prepend('===== Select =====', '');
        $customerloclist  = CustomerLoc::pluck('name', 'id');
        $customerloclist->prepend('===== Select =====', '');
        $productlist  = Product::pluck('sname', 'id');
        return view('orders.edit', compact('order_m', 'customerlist', 'customerloclist', 'productlist'));
    }

    public function editAction(Request $request, $order_m_id){
        $requestData = $request->all();

        $order_m = OrderM::findOrFail($order_m_id);

        //Create main data 
        $orderMObj = array();

        $orderMObj['order_date'] = $requestData['order_date'];
        $orderMObj['customer_id'] = $requestData['customer_id'];
        $orderMObj['customer_loc_id'] = $requestData['customer_loc_id'];
        $orderMObj['po_code'] = $requestData['po_code'];
        $orderMObj['so_code'] = $requestData['so_code'];
        $orderMObj['billing_code'] = $requestData['billing_code'];
        $orderMObj['inv_code'] = $requestData['inv_code'];
        $orderMObj['note'] = $requestData['note'];

        $order_m->update($orderMObj);

        $productpricelist = ProductPrice::where('customer_loc_id', $requestData['customer_loc_id'])->pluck('base_price', 'product_id');

        // var_dump($productpricelist);

        $chk = StockM::where('ref_id', $order_m_id)->first();

        $stockmid = 0;

        if(empty($chk)){

            $tmp = array();
            $tmp['stock_datetime'] =  $requestData['order_date'] . " 00:00:00.000";
            $tmp['log'] = "Order Date " . $requestData['order_date'] . " Customer Loc id " . $requestData['customer_loc_id'];
            $tmp['sap_link'] = "";
            $tmp['desc'] = "Order Date " . $requestData['order_date'] . " Customer Loc id " . $requestData['customer_loc_id'];
            $tmp['ref_id']  = $order_m_id;
            $tmp['ref_from'] = "ORDER";

            $stockmid = StockM::create($tmp)->id;

        }else{
            $chk->stock_datetime =  $requestData['order_date'] . " 00:00:00.000";
            $chk->log = "Order Date " . $requestData['order_date'] . " Customer Loc id " . $requestData['customer_loc_id'];
            $chk->sap_link = "";
            $chk->desc = "Order Date " . $requestData['order_date'] . " Customer Loc id " . $requestData['customer_loc_id'];
            $chk->ref_id  = $order_m_id;
            $chk->ref_from = "ORDER";

            $chk->update();

            $stockmid = $chk->id;
        }


        $productlist  = Product::all();

        StockD::where('stock_m_id', $stockmid)->delete();
        foreach ($productlist as $productObj) {
            $key = $productObj->id;
            $orderd = OrderD::where('order_m_id', $order_m_id)->where('product_id', $key)->first();

            

            if (!empty($requestData['numpack_' . $key])) {
                $orderDObj = array();
                $orderDObj['order_m_id'] = $order_m_id;
                $orderDObj['product_id'] = $key;
                $orderDObj['customer_loc_id'] = 0;
                $orderDObj['value'] = $requestData['numpack_' . $key];
                $orderDObj['weight'] = $productObj->package_weight * $requestData['numpack_' . $key];

                $orderDObj['current_price'] = 0;
                //echo "product " . $productObj->id;
                if (isset($productpricelist[$productObj->id])) {
                    $orderDObj['current_price'] = $productpricelist[$productObj->id];
                   // echo "set ". $productpricelist[$productObj->id];
                }

                $stockDObj = array();

                $stockDObj['stock_m_id'] = $stockmid;
                $stockDObj['product_id'] = $key;
                $stockDObj['value'] = -1 * $requestData['numpack_' . $key];

                var_dump($stockDObj);

                StockD::create($stockDObj);

                if(!empty($orderd)){
                    $orderd->update($orderDObj);
                }else{
                    $orderdid = OrderD::create($orderDObj);
                }
                
            }else{
                if (!empty($orderd)) {
                    $orderd->delete();
                }
            }
        }

       return redirect('orders/index')->with('flash_message', ' updated!');

    }

    public function delete($order_m_id){
        OrderD::where('order_m_id', $order_m_id)->delete();

        OrderM::destroy($order_m_id);

        return redirect('orders/index')->with('flash_message', ' deleted!');
    }

    public function addDetail($id){
        
    }

    public function getOrderInDateInCar(Request $request)
    {
        $dateval = $request->get('dateval');

        $orderms = OrderM::where('order_date', $dateval)->get();

        $tmpResult = "";
        if(!empty($orderms) && $orderms->count() > 0){
            $tmpResult = "<table class='table'><thead><tr>
                    <th>Order date</th>
                    <th>ลูกค้า / สาขา</th>
                    <th>สินค้า / จำนวน</th></tr>
                    </thead><tbody>";
            foreach ($orderms as $ordermobj) {
                $tmpResult .= "<tr><td>" . $ordermobj->order_date . "</td>";
                $tmpResult .= "<td>" . $ordermobj->customer->name . " / " . $ordermobj->customerloc->name . "</td>";
                $tmpResult .= "<td>" ;
                foreach ($ordermobj->orderd as $orderdobj) {
                    $tmpResult .= $orderdobj->product->name . " (" . $orderdobj->value . ") <br/>";
                }
                $tmpResult .= "</td></tr>";
            }
            $tmpResult .= "</tbody></table>";
        }else{
            $tmpResult = "<table class='table'><thead><tr>
                    <th>ไม่มีข้อมูล</th></tr>
                    </thead></table>";
        }
        return $tmpResult;
    }

    
}
