<div class="row">

    <div class="col-md-4  {{ $errors->has('customer_id') ? 'has-error' : ''}}">
        {!! Form::label('customer_id', 'ลูกค้า', ['class' => 'control-label']) !!}
        @if (isset($order_m->customer_id))
            {!! Form::select('customer_id', $customerlist,$order_m->customer_id, ['class' => 'form-control caldate']) !!}   
        @else
            {!! Form::select('customer_id', $customerlist,null, ['class' => 'form-control caldate']) !!}
        @endif
        {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('customer_loc_id') ? 'has-error' : ''}}">
        {!! Form::label('customer_loc_id', 'สาขา', ['class' => 'control-label']) !!}
        @if (isset($order_m->customer_loc_id))
            {!! Form::select('customer_loc_id', $customerloclist,$order_m->customer_loc_id, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('customer_loc_id', $customerloclist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('customer_loc_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('order_date') ? 'has-error' : ''}}">
    <label for="order_date" class="control-label">{{ 'Order Date' }}</label>
    @if (isset($order_m->order_date))
        {!! Form::Date('order_date',  $order_m->order_date, ['id' => 'order_date','class' => 'getorderlist form-control']) !!}
    @else
        {!! Form::Date('order_date',  \Carbon\Carbon::now(), ['id' => 'order_date','class' => 'getorderlist form-control']) !!}
    @endif
    
    {!! $errors->first('order_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('po_code') ? 'has-error' : ''}}">
    <label for="po_code" class="control-label">{{ 'PO' }}</label>
    <input class="form-control" name="po_code" type="text" id="po_code" value="{{ $order_m->po_code or ''}}" >
    {!! $errors->first('po_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('so_code') ? 'has-error' : ''}}">
    <label for="so_code" class="control-label">{{ 'SO' }}</label>
    <input class="form-control" name="so_code" type="text" id="so_code" value="{{ $order_m->so_code or ''}}" >
    {!! $errors->first('so_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('billing_code') ? 'has-error' : ''}}">
    <label for="billing_code" class="control-label">{{ 'Billing' }}</label>
    <input class="form-control" name="billing_code" type="text" id="billing_code" value="{{ $order_m->billing_code or ''}}" >
    {!! $errors->first('billing_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('inv_code') ? 'has-error' : ''}}">
    <label for="inv_code" class="control-label">{{ 'INV' }}</label>
    <input class="form-control" name="inv_code" type="text" id="inv_code" value="{{ $order_m->inv_code or ''}}" >
    {!! $errors->first('inv_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('note') ? 'has-error' : ''}}">
    <label for="note" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="note" id="note">{{ $order_m->note or ''}}</textarea>
    {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
</div>

@foreach ($productlist as $key=>$value)
    <div class="col-md-3 {{ $errors->has('numpack_'.$key) ? 'has-error' : ''}}">
        {!! Form::label('numpack_'.$key, $value, ['class' => 'control-label']) !!}
        ราคา <span id="price_{{$key}}" data-id="{{$key}}" class="setprice">
            @if (isset($pricedata[$key]))
                {{ $pricedata[$key] }}
            @else
                0.00
            @endif
        </span> 
        จำนวน <span id="stock_{{$key}}"  data-id="{{$key}}" class="setstock" >
            @if (isset($stockdata[$key]))
                {{ $stockdata[$key] }}
            @else
                0
            @endif</span>
        @if (!empty($order_m) && $order_m->orderd->count() > 0)
            @php
            $flag = false;    
            @endphp
            @foreach ($order_m->orderd as $itemd)
                @if ($itemd->product_id == $key)
                    @php
                        $flag = true;
                    @endphp
                    <input class="form-control" name='numpack_{{ $key }}' type="number" id='numpack_{{ $key }}' value="{{ $itemd->value }}" >
                @endif        
            @endforeach
            @if (!$flag)
                   <input class="form-control" name='numpack_{{ $key }}' type="number" id='numpack_{{ $key }}' value="0" >
            @endif
        @else
            <input class="form-control" name='numpack_{{ $key }}' type="number" id='numpack_{{ $key }}' value="0" >
        @endif

    
        {!! $errors->first('numpack_'.$key, '<p class="help-block">:message</p>') !!}
</div>
@endforeach


<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

<div class="col-md-12">
    <div id="resultorder">
    </div>
</div>
</div>
