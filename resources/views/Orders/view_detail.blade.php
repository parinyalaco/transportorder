@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Order {{ $orderm->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/orders/index') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        
                        
                        <br/>

                        <div class="row">
                            <div class="col-md-4"><b>วันที่</b> {{ $orderm->order_date }}</div>
                            <div class="col-md-4"><b>ลูกค้า/สาขา</b> {{ $orderm->customer->name }} / {{ $orderm->customerloc->name }}</div>
                        </div>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>สินค้า</th>
                                        <th>จำนวน</th>
                                        <th>ราคาต่อหน่วย</th>
                                        <th>ราคา</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $totalprice = 0;
                                    @endphp
                                    @foreach ($orderm->orderd as $item)
                                       <tr>
                                        <td>{{ $item->product->productgroup->name }} / {{ $item->product->name }}</td>
                                        <td>{{ $item->value }}</td>
                                         
                                        @if(!empty($item->product->productpricebylocid($orderm->customerloc->id)))
                                            <td>
                                                {{ $item->product->productpricebylocid($orderm->customerloc->id)->base_price or 0 }}
                                            </td>
                                            <td>
                                                {{ number_format($item->product->productpricebylocid($orderm->customerloc->id)->base_price * $item->value,2) }}
                                            </td>
                                            @php
                                                $totalprice += $item->product->productpricebylocid($orderm->customerloc->id)->base_price * $item->value
                                            @endphp
                                        @else
                                            <td>0.00</td>
                                            <td>0.00</td>
                                        @endif    
                                        </td>
                                    </tr> 
                                    @endforeach
                                    <tr>
                                        <td colspan="3"><b>Total</b></td>
                                        <td><b>{{ number_format($totalprice,2) }}</b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
