<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Customer;
use App\CustomerLoc;
use Illuminate\Http\Request;

class CustomerLocsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $customerlocs = CustomerLoc::latest()->paginate($perPage);
        } else {
            $customerlocs = CustomerLoc::latest()->paginate($perPage);
        }

        return view('customer-locs.index', compact('customerlocs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $customerlist  = Customer::pluck('name', 'id');
        return view('customer-locs.create',compact('customerlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        CustomerLoc::create($requestData);

        return redirect('customer-locs')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $customerloc = CustomerLoc::findOrFail($id);

        return view('customer-locs.show', compact('customerloc'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $customerlist  = Customer::pluck('name', 'id');
        $customerloc = CustomerLoc::findOrFail($id);

        return view('customer-locs.edit', compact('customerloc', 'customerlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $customerloc = CustomerLoc::findOrFail($id);
        $customerloc->update($requestData);

        return redirect('customer-locs')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        CustomerLoc::destroy($id);

        return redirect('customer-locs')->with('flash_message', ' deleted!');
    }

    public function calnexttrans(Request $request){
        $select = $request->get('select');
        $value = $request->get('value');
        $customerloc = CustomerLoc::findOrFail($value);

        $ntoday = strtotime($customerloc->cond);

        if(date('Y-m-d', $ntoday) == date('Y-m-d')){
            echo date('Y-m-d');
        }else{
            $nextdate = strtotime('Next ' . $customerloc->cond);
            echo date('Y-m-d', $nextdate);
        }

        
    }
}
