<div class="row">

    <div class="col-md-3  {{ $errors->has('customer_id') ? 'has-error' : ''}}">
        {!! Form::label('customer_id', 'ลูกค้า', ['class' => 'control-label']) !!} : {{ $tranm->orderm->customer->name }}
        {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3 {{ $errors->has('customer_loc_id') ? 'has-error' : ''}}">
        {!! Form::label('customer_loc_id', 'สาขา', ['class' => 'control-label']) !!} : {{ $tranm->orderm->customerloc->name }}
        {!! $errors->first('customer_loc_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('order_date') ? 'has-error' : ''}}">
    <label for="order_date" class="control-label">{{ 'แผนส่งของ' }}</label> : {{ $tranm->orderm->order_date }}
    {!! $errors->first('order_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('seq') ? 'has-error' : ''}}">
    <label for="seq" class="control-label">{{ 'ลำดับ' }}</label> : {{ $tranm->seq or 0 }}
    {!! $errors->first('seq', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-3  {{ $errors->has('tran_date') ? 'has-error' : ''}}">
    <label for="tran_date" class="control-label">{{ 'วันส่งของ' }}</label>
    @if (isset($tranm->tran_date))
        {!! Form::Date('tran_date',  $tranm->tran_date, ['id' => 'tran_date','class' => 'form-control']) !!}
    @else
        {!! Form::Date('tran_date',  \Carbon\Carbon::now(), ['id' => 'tran_date','class' => 'form-control']) !!}
    @endif
    
    {!! $errors->first('tran_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('car') ? 'has-error' : ''}}">
    <label for="seq" class="control-label">{{ 'รถ' }}</label> : {{ $tranm->car->license_no or '-' }}
    {!! $errors->first('car', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('driver') ? 'has-error' : ''}}">
    <label for="driver" class="control-label">{{ 'คนขับ' }}</label> : {{ $tranm->driver->nickname or '-' }}
    {!! $errors->first('driver', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('support') ? 'has-error' : ''}}">
    <label for="support" class="control-label">{{ 'ผู้ช่วย' }}</label> : {{ $tranm->support->nickname or '-' }}
    {!! $errors->first('support', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12">
    <div id="resultorder">
        <table class="table">
            <thead>
                <tr>
                    <th>สินค้า</th>
                    <th>จำนวนที่สั่ง</th>
                    <th>จำนวนที่ส่ง</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tranm->trand as $item)
                <tr>
                    <td>{{ $item->orderd->product->productgroup->name }} {{ $item->orderd->product->name }}</td>
                    <td>{{ $item->orderd->value }}</td>
                    <td>
                    <input class="form-control" name="accept_value{{$item->id}}" type="number" id="accept_value{{$item->id}}" value="{{ $item->accept_value or $item->orderd->value }}" > 
                    </td>
                </tr>     
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="col-md-3 {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'สถานะ', ['class' => 'control-label']) !!}
        {!! Form::select('status', $statuslist,$tranm->status, ['class' => 'form-control']) !!}   
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-9  {{ $errors->has('note') ? 'has-error' : ''}}">
    <label for="note" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="note" id="note">{{ $tranm->note or ''}}</textarea>
    {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>


</div>
