<div class="row">

    <div class="col-md-3  {{ $errors->has('customer_id') ? 'has-error' : ''}}">
        {!! Form::label('customer_id', 'ลูกค้า', ['class' => 'control-label']) !!} : {{ $tranm->orderm->customer->name }}
        {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3 {{ $errors->has('customer_loc_id') ? 'has-error' : ''}}">
        {!! Form::label('customer_loc_id', 'สาขา', ['class' => 'control-label']) !!} : {{ $tranm->orderm->customerloc->name }}
        {!! $errors->first('customer_loc_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('order_date') ? 'has-error' : ''}}">
    <label for="order_date" class="control-label">{{ 'แผนส่งของ' }}</label> : {{ $tranm->orderm->order_date }}
    {!! $errors->first('order_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('seq') ? 'has-error' : ''}}">
    <label for="seq" class="control-label">{{ 'ลำดับ' }}</label> : {{ $tranm->seq or 0 }}
    {!! $errors->first('seq', '<p class="help-block">:message</p>') !!}
</div>
<div class="row" >
<div class="col-md-9 {{ $errors->has('set_id') ? 'has-error' : ''}}">
        {!! Form::label('set_id', 'กลุ่มที่จัดแล้ว', ['class' => 'control-label']) !!}
        @if (isset($tranm->car_id))
            {!! Form::select('set_id', $setlist,$tranm->set_id, ['id' => 'set_id', 'class' => 'form-control']) !!}   
        @else
            {!! Form::select('set_id', $setlist,null, ['id' => 'set_id' ,'class' => 'form-control']) !!}
        @endif
        {!! $errors->first('set_id', '<p class="help-block">:message</p>') !!}
</div>
</div>
<div class="row" >



<div class="col-md-3 {{ $errors->has('car_id') ? 'has-error' : ''}}">
        {!! Form::label('car_id', 'รถ', ['class' => 'control-label']) !!}
        @if (isset($tranm->car_id))
            {!! Form::select('car_id', $carlist,$tranm->car_id, ['id' => 'car_id', 'class' => 'form-control']) !!}   
        @else
            {!! Form::select('car_id', $carlist,null, ['id' => 'car_id' ,'class' => 'form-control']) !!}
        @endif
        {!! $errors->first('car_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3 {{ $errors->has('driver_id') ? 'has-error' : ''}}">
        {!! Form::label('driver_id', 'คนขับ', ['class' => 'control-label']) !!}
        @if (isset($tranm->driver_id))
            {!! Form::select('driver_id', $driverlist,$tranm->driver_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('driver_id', $driverlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('driver_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3 {{ $errors->has('support_id') ? 'has-error' : ''}}">
        {!! Form::label('support_id', 'ผู้ช่วย', ['class' => 'control-label']) !!}
        @if (isset($tranm->support_id))
            {!! Form::select('support_id', $driverlist,$tranm->support_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('support_id', $driverlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('support_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3  {{ $errors->has('tran_date') ? 'has-error' : ''}}">
    <label for="tran_date" class="control-label">{{ 'วันส่งของ' }}</label>
    @if (isset($tranm->tran_date))
        {!! Form::Date('tran_date',  $tranm->tran_date, ['id' => 'tran_date','class' => 'form-control']) !!}
    @else
        {!! Form::Date('tran_date',  \Carbon\Carbon::now(), ['id' => 'tran_date','class' => 'form-control']) !!}
    @endif
    
    {!! $errors->first('tran_date', '<p class="help-block">:message</p>') !!}
</div>
</div>
<div class="col-md-12">
    <div id="resultorder">
        <table class="table">
            <thead>
                <tr>
                    <th>สินค้า</th>
                    <th>จำนวน</th>
                    <th>น้ำหนัก (kg)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tranm->orderm->orderd as $item)
                <tr>
                    <td>{{ $item->product->productgroup->name }} {{ $item->product->name }}</td>
                    <td>{{ $item->value }}</td>
                    <td>{{ number_format($item->weight, 2, '.', ',') }}</td>
                </tr>     
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="col-md-12  {{ $errors->has('note') ? 'has-error' : ''}}">
    <label for="note" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="note" id="note">{{ $tranm->note or ''}}</textarea>
    {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>


</div>
