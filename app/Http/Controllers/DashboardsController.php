<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\OrderM;
use App\TranM;

class DashboardsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $orderData = OrderM::groupBy('order_date')
        ->select(DB::raw(
            'order_date,
            count(id) as countdata'))
        ->get();


        $tranDataRw = TranM::groupBy('status')
            ->groupBy('tran_date')
            ->select(DB::raw(
            'tran_date,
            status,
            count(id) as countdata'
            ))
            ->get();

        

        $tranData = array();
        foreach ($tranDataRw as $tranDataRwObj) {
            $tranData[$tranDataRwObj->tran_date][$tranDataRwObj->status] = $tranDataRwObj;
        }

        //print_r($tranData);

        $statusList = array(
            'Pending',
            'Active',
            'Accept-ALL',
            'Accept-Some',
            'Reject'
        );

        return view('dashboards.index', compact('orderData','tranData', 'statusList'));
    }
}
