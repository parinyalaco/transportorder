<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerLoc extends Model
{
    protected $fillable = [
        'customer_id'
      ,'name'
      ,'sap_code'
      ,'code'
      ,'desc'
      ,'cond'
      ,'contract_sap'
      ,'lat'
      ,'long'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id', 'id');
    }

    public function order()
    {
        return $this->hasMany('App\OrderM', 'customer_loc_id');
    }
    
}
