@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="row">
                        @if (Auth::user()->role->name == 'admin')
                            ADMIN
                        @else
                            @if (Auth::user()->role->name == 'sale')
                                Sale
                            @else
                                @if (Auth::user()->role->name == 'driver')
                                    driver
                                @else
                                    @if (Auth::user()->role->name == 'report')
                                        report
                                    @else
                                        planner
                                    @endif
                                @endif
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
