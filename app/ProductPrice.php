<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    protected $fillable = ['product_id', 'customer_loc_id', 'base_price', 'vat_price', 'pro_price', 'start_date', 'end_date'
    ];

    public function customerloc()
    {
        return $this->belongsTo('App\CustomerLoc', 'customer_loc_id', 'id');
    }

}
