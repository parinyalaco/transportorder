<div class="row">

    <div class="form-group col-md-4 {{ $errors->has('stock_datetime') ? 'has-error' : ''}}">
        <label for="stock_datetime" class="control-label">{{ 'วัน-เวลา' }}</label>
        <input class="form-control" name="stock_datetime" type="datetime-local" id="stock_datetime" 
        value = "@php
            if(isset($stock->stock_datetime)){
                echo date('Y-m-d\TH:i',strtotime($stock->stock_datetime));
            }else{
                echo \Carbon\Carbon::now()->format('Y-m-d\TH:i');
            }
        @endphp" >
        {!! $errors->first('stock_datetime', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-4  {{ $errors->has('log') ? 'has-error' : ''}}">
    <label for="log" class="control-label">{{ 'Log' }}</label>
    <input class="form-control" name="log" type="text" id="log" value="{{ $stock->log or ''}}" >
    {!! $errors->first('log', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-4  {{ $errors->has('sap_link') ? 'has-error' : ''}}">
    <label for="sap_link" class="control-label">{{ 'SAP Link' }}</label>
    <input class="form-control" name="sap_link" type="text" id="sap_link" value="{{ $stock->sap_link or ''}}" >
    {!! $errors->first('log', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'Desc' }}</label>
    <input class="form-control" name="desc" type="text" id="desc" value="{{ $stock->desc or ''}}" >
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    @foreach ($productlist as $key=>$value)
    <div class="col-md-3 {{ $errors->has('numpack_'.$key) ? 'has-error' : ''}}">
        {!! Form::label('numpack_'.$key, $value, ['class' => 'control-label']) !!}
        @if (!empty($stock) && $stock->stockd->count() > 0)
            @php
            $flag = false;    
            @endphp
            @foreach ($stock->stockd as $itemd)
                @if ($itemd->product_id == $key)
                    @php
                        $flag = true;
                    @endphp
                    <input class="form-control" name='numpack_{{ $key }}' type="number" id='numpack_{{ $key }}' value="{{ $itemd->value }}" >
                @endif        
            @endforeach
            @if (!$flag)
                   <input class="form-control" name='numpack_{{ $key }}' type="number" id='numpack_{{ $key }}' value="0" >
            @endif
        @else
            <input class="form-control" name='numpack_{{ $key }}' type="number" id='numpack_{{ $key }}' value="0" >
        @endif

    
        {!! $errors->first('numpack_'.$key, '<p class="help-block">:message</p>') !!}
</div>
@endforeach

</div>
<div class="row">
<div class="form-group col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
</div>
