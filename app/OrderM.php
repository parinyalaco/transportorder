<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderM extends Model
{
    protected $fillable = [
        'order_date', 'customer_id', 'customer_loc_id', 'po_code', 'so_code', 'billing_code', 'inv_code', 'status', 'note'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id', 'id');
    }

    public function customerloc()
    {
        return $this->belongsTo('App\CustomerLoc', 'customer_loc_id', 'id');
    }

    public function orderd()
    {
        return $this->hasMany('App\OrderD', 'order_m_id');
    }

    public function tranm()
    {
        return $this->hasMany('App\TranM', 'order_m_id');
    }
}
